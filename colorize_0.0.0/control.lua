--require "defines"
require "util"

script.on_event(defines.events.on_tick, function()
		build_gui()
end)

color_value_base = 255

--------------------------------------------------------------------------------------
script.on_event(defines.events.on_gui_click, function(event) 
	if event.element.name == "btn_ss" then
		game.players[event.player_index].force.set_spawn_position(game.players[event.player_index].position,game.players[event.player_index].surface)
	end
	
	if event.element.name == "btn_sc" then
		local red_value = tonumber(player.gui.top.hmf.hcf.txt_hctr.text)
		local green_value = tonumber(player.gui.top.hmf.hcf.txt_hctg.text)
		local blue_value = tonumber(player.gui.top.hmf.hcf.txt_hctb.text)
		local alpha_value = tonumber(player.gui.top.hmf.hcf.txt_hcta.text)
		
		if red_value ~= nil and green_value ~= nil and blue_value ~= nil then
		
			if red_value > color_value_base then 
				red_value = color_value_base 
			elseif red_value < 0 then 
				red_value = 0 
			end
			
			if green_value > color_value_base then 
				green_value = color_value_base 
			elseif green_value < 0 then 
				green_value = 0 
			end
			
			if blue_value > color_value_base then 
				blue_value = color_value_base 
			elseif blue_value < 0 then 
				blue_value = 0 
			end
			
			if alpha_value > color_value_base then 
				alpha_value = color_value_base 
			elseif alpha_value < 0 then 
				alpha_value = 0 
			end
			
			player.color = {
				r = (red_value / color_value_base), 
				g = (green_value / color_value_base), 
				b = (blue_value / color_value_base), 
				a = (alpha_value / color_value_base)}
	end

	if event.element.name == "chk_hcsc" then
		update_gui(event.player_index)
	end
end)

--------------------------------------------------------------------------------------
function build_gui()
	local player
	
	for _, player in pairs(game.players) do
		if player.connected then
			if player.gui.top.hmf == nil then	
				player.gui.top.add(
				{
					type = "frame", 
					name = "hmf", 
					caption = "", 
					direction = "vertical"
				})
			end
			
			local gui_main = player.gui.top.hmf
			
			if gui_main.hcf == nil then	
				local gui_hcf = gui_main.add(
				{
					type = "flow", 
					name = "hcf", 
					direction = "horizontal"
				})
				gui_hcf.add(
				{
					type = "label", 
					name = "lbl_hcl", 
					caption = "colorize" , 
					font_color = white
				})	
				gui_hcf.add(
				{
					type = "checkbox", 
					name = "chk_hcsc", 
					state = false
				})									
			end
		end
	end
end

--------------------------------------------------------------------------------------
function update_gui(player_index)
	local player = game.players[player_index]
	--This is assuming where this is being called the proper gui containers are made already
	local guif = player.gui.top.hmf.hcf
	if not guif.chk_hcsc.state then 
		if guif.txt_hctr then guif.txt_hctr.destroy() end
		if guif.txt_hctg then guif.txt_hctg.destroy() end
		if guif.txt_hctb then guif.txt_hctb.destroy() end
		if guif.txt_hcta then guif.txt_hcta.destroy() end
		if guif.lbl_desc then guif.lbl_desc.destroy() end
		if guif.btn_sc then guif.btn_sc.destroy() end
		if guif.btn_ss then guif.btn_ss.destroy() end
	else
		guif.add({type = "textfield", name = "txt_hctr", caption= "Red", font_color = white}) 
		guif.add({type = "textfield", name = "txt_hctg", caption = "Green" , font_color = white}) 
		guif.add({type = "textfield", name = "txt_hctb", caption = "Blue" , font_color = white}) 
		guif.add({type = "textfield", name = "txt_hcta", caption = "Alpha" , font_color = white})
		guif.add({type = "label", name = "lbl_desc", caption = ("0-255 RGB"), font_color = white}) 
		guif.add({type = "button",caption = "Set Color",name = "btn_sc",style="button_style"})	
		guif.add({type = "button",caption = "Set Spawn",name = "btn_ss",style="button_style"})	
		--Set the current player color values 
		guif.txt_hctr.text = tostring(math.floor(player.color.r * color_value_base))		
		guif.txt_hctg.text = tostring(math.floor(player.color.g * color_value_base))		
		guif.txt_hctb.text = tostring(math.floor(player.color.b * color_value_base))		 
		guif.txt_hcta.text = tostring(math.floor(player.color.a * color_value_base))		
	end
end