local default_gui = data.raw["gui-style"].default

default_gui.color_frame_style = 
{
	type="frame_style",
	parent="frame_style",
	top_padding = 0,
	right_padding = 0,
	bottom_padding = 0,
	left_padding = 0,
}

default_gui.color_text_style =
{
	type="textfield_style",
	parent="default",
	default_font_color={r=1, g=1, b=1},
	hovered_font_color={r=1, g=1, b=1},
	minimal_width=35,
	maximal_width=35,
	minimal_height=20,
	maximal_height=20,
	top_padding = 0,
	right_padding = 0,
	bottom_padding = 0,
	left_padding = 0,
}

default_gui.color_button_style =
{
	type="button_style",
	parent="default",
	default_font_color={r=1, g=1, b=1},
	hovered_font_color={r=1, g=1, b=1},
	minimal_width=20,
	maximal_width=20,
	minimal_height=20,
	maximal_height=20,
	top_padding = 0,
	right_padding = 0,
	bottom_padding = 0,
	left_padding = 0,
}


