item_htd_weapon_tech

[OBJECT:ITEM]

cylinder rifle
[ITEM_WEAPON:ITEM_HTD_WEAPON_CYLINDER_RIFLE] should probably be only iron or steel
	[NAME:bayonet cylinder rifle:bayonet cylinder rifles]
	[SIZE:2000]
	[SKILL:PIKE]
	[RANGED:BOW:SLUG]CROSSBOW BLOWGUN
		[SHOOT_FORCE:15000]
		[SHOOT_MAXVEL:20000]
	[TWO_HANDED:65000]
	[MINIMUM_SIZE:60000]
	[MATERIAL_SIZE:10]
	[ATTACK:EDGE:50:15000:stab:stabs:bayonet:2000]
		[ATTACK_PREPARE_AND_RECOVER:3:5]
	[ATTACK:BLUNT:100000:8000:slap:slaps:barrel:2250]
		[ATTACK_PREPARE_AND_RECOVER:3:5]
	[ATTACK:BLUNT:10000:4000:bash:bashes:stock:2250]
		[ATTACK_PREPARE_AND_RECOVER:4:5]

break rifle
[ITEM_WEAPON:ITEM_HTD_WEAPON_BREAK_RIFLE] should probably be only iron or steel
	[NAME:rifle:rifles]
	[ADJECTIVE:break action]
	[SIZE:500]
	[SKILL:HAMMER]
	[RANGED:BOW:PELLET]CROSSBOW BLOWGUN
		[SHOOT_FORCE:5000]
		[SHOOT_MAXVEL:10000]
	[TWO_HANDED:0]
	[MINIMUM_SIZE:15000]
	[MATERIAL_SIZE:3]
	[ATTACK:BLUNT:100000:8000:slap:slaps:barrel:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:10000:4000:bash:bashes:stock:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		
[ITEM_WEAPON:ITEM_HTD_WEAPON_BREAK_RIFLE_B] should probably be only iron or steel
	[NAME:bayonet break action rifle:bayonet break action rifles]
	[SIZE:600]
	[SKILL:HAMMER]
	[RANGED:BOW:PELLET]CROSSBOW BLOWGUN
		[SHOOT_FORCE:5000]
		[SHOOT_MAXVEL:10000]
	[TWO_HANDED:0]
	[MINIMUM_SIZE:15000]
	[MATERIAL_SIZE:4]
	[ATTACK:EDGE:20:10000:stab:stabs:bayonet:1000]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:100000:8000:slap:slaps:barrel:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:10000:4000:bash:bashes:stock:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		
pistols and combos
[ITEM_WEAPON:ITEM_HTD_WEAPON_PISTOL]
	[NAME:pistol:pistols]
	[ADJECTIVE:flintlock]
	[SIZE:200]
	[SKILL:MACE] -if used as a melee
	[RANGED:BLOWGUN:PELLET]
		[SHOOT_FORCE:2000]
		[SHOOT_MAXVEL:10000]
	[TWO_HANDED:0]
	[MINIMUM_SIZE:5000]
	[MATERIAL_SIZE:1]
	[ATTACK:BLUNT:10000:4000:club:clubs:handle:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		
[ITEM_WEAPON:ITEM_HTD_WEAPON_PISTOL_SS]
	[NAME:short sword and flintlock pistol:short swords and flintlock pistols]
	[SIZE:500]
	[SKILL:SWORD] -if used as a melee
	[RANGED:BLOWGUN:PELLET]
		[SHOOT_FORCE:2000]
		[SHOOT_MAXVEL:10000]
	[TWO_HANDED:37500]
	[MINIMUM_SIZE:32500]
	[MATERIAL_SIZE:4]
	[ATTACK:EDGE:20000:4000:slash:slashes:sword:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:EDGE:50:2000:stab:stabs:sword:1000]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:20000:4000:slap:slaps:flat:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:100:1000:strike:strikes:sword pommel:1000]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
		
[ITEM_WEAPON:ITEM_HTD_WEAPON_PISTOL_LD]
	[NAME:dagger and flintlock pistol:daggers and flintlock pistols]
	[ADJECTIVE:large]
	[SIZE:400]
	[SKILL:DAGGER] -if used as a melee
	[RANGED:BLOWGUN:PELLET]
		[SHOOT_FORCE:2000]
		[SHOOT_MAXVEL:10000]
	[TWO_HANDED:27500]
	[MINIMUM_SIZE:5000]
	[MATERIAL_SIZE:2]
	[ATTACK:EDGE:1000:800:slash:slashes:dagger:1250]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:EDGE:5:1000:stab:stabs:dagger:1000]
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:20:600:strike:strikes:dagger pommel:1000]
		[ATTACK_PREPARE_AND_RECOVER:3:3]