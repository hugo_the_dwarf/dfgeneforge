item_htd_weapon_dwarf

[OBJECT:ITEM]

[ITEM_WEAPON:ITEM_HTD_WEAPON_1H_AXE_WAR_DWARF]
	[NAME:dwarven war axe:dwarven war axes]
	[ADJECTIVE:bearded](adj) (material) (name)
	[SIZE:680]
	[SKILL:SWORD] -- Dwarves don't use swords, my dwarves at least
	[TWO_HANDED:60000] --dwarves are fine
	[MINIMUM_SIZE:47500]
	[MATERIAL_SIZE:3]

	The format is ATTACK:EDGE/BLUNT:contact area:penetration size:verb2nd:verb3rd:noun:velocity multiplier
	Penetration size currently only matters for edged attacks.
	
	I need to do da maths

	[ATTACK:EDGE:10000:6000:hack:hacks:NO_SUB:1250]
		[ATTACK_PREPARE_AND_RECOVER:2:1]
		[ATTACK_FLAG_INDEPENDENT_MULTIATTACK] --does this mean I can hack and slap with the same axe without issue? or can I hack with two axes at once?

	[ATTACK:BLUNT:15000:100:slap:slaps:flat:1250]
		[ATTACK_PREPARE_AND_RECOVER:2:1]
		[ATTACK_FLAG_INDEPENDENT_MULTIATTACK]
		
	[ATTACK:BLUNT:500:100:jab:jabs:head:1000]
		[ATTACK_PREPARE_AND_RECOVER:1:1]
		[ATTACK_FLAG_INDEPENDENT_MULTIATTACK]
		
	[ATTACK:BLUNT:100:100:strike:strikes:pommel:1000]
		[ATTACK_PREPARE_AND_RECOVER:1:1]
		[ATTACK_FLAG_INDEPENDENT_MULTIATTACK]



[ITEM_WEAPON:ITEM_HTD_WEAPON_2H_AXE_BATTLE_DWARF]
	[NAME:dwarven battle axe:dwarven battle axes]
	[ADJECTIVE:bearded](adj) (material) (name)
	[SIZE:1280]
	[SKILL:AXE]
	[TWO_HANDED:70000] -- due to it being smaller because made for dwarves, 1h for humans
	[MINIMUM_SIZE:42500]
	[MATERIAL_SIZE:6]
	[ATTACK:EDGE:10000:6000:hack:hacks:NO_SUB:2750] -- more weight to use as momentum
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:15000:100:slap:slaps:flat:2750] -two headed more area to slap with
		[ATTACK_PREPARE_AND_RECOVER:3:3]
	[ATTACK:BLUNT:520:100:jab:jabs:head:1000]
		[ATTACK_PREPARE_AND_RECOVER:1:1]
	[ATTACK:BLUNT:55:100:strike:strikes:pommel:1000]
		[ATTACK_PREPARE_AND_RECOVER:1:1]