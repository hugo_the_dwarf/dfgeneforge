/c game.player.character.surface.create_entity{name = "market", position = game.player.character.surface.find_non_colliding_position("market", game.player.character.position, 10, 1)}



/c local player = game.player player.insert{name="submachine-gun"} player.insert{name="uranium-rounds-magazine", count=500}  player.insert{name="power-armor-mk2", count = 1}  local p_armor = player.get_inventory(5)[1].grid   p_armor.put({name = "fusion-reactor-equipment"})   p_armor.put({name = "fusion-reactor-equipment"})   p_armor.put({name = "fusion-reactor-equipment"})   p_armor.put({name = "exoskeleton-equipment"})   p_armor.put({name = "exoskeleton-equipment"})   p_armor.put({name = "exoskeleton-equipment"})   p_armor.put({name = "exoskeleton-equipment"})   p_armor.put({name = "energy-shield-mk2-equipment"})   p_armor.put({name = "energy-shield-mk2-equipment"})   p_armor.put({name = "personal-roboport-mk2-equipment"})   p_armor.put({name = "night-vision-equipment"})   p_armor.put({name = "battery-mk2-equipment"})   p_armor.put({name = "battery-mk2-equipment"})
/c game.player.insert{name="submachine-gun"}
/c game.player.insert{name="uranium-rounds-magazine", count=500}


require("silo-script")
local version = 1

script.on_event(defines.events.on_tick, function()
	build_gui()
	if game.tick % 60 then
		update_shop_items()
	end
	
	if global.player_credits == nil then 
		global.player_credits = {}
	end
	
	for _, player in pairs(game.players) do
		if player.connected then			
			if global.player_credits[player.index] == nil then
				--game.print("setting player " .. player.index .. "'s credits from nil to 0")
				global.player_credits[player.index] = 0
			else
				if player.gui.top.hmf.hcf.lbl_cred then
					player.gui.top.hmf.hcf.lbl_cred.caption = global.player_credits[player.index]
				end
			end
		end
	end
	
end)

script.on_event(defines.events.on_player_created, function(event)
  local player = game.players[event.player_index]
  player.insert{name="iron-plate", count=8}
  player.insert{name="pistol", count=1}
  player.insert{name="firearm-magazine", count=10}
  player.insert{name="burner-mining-drill", count = 1}
  player.insert{name="stone-furnace", count = 1}
  player.force.chart(player.surface, {{player.position.x - 200, player.position.y - 200}, {player.position.x + 200, player.position.y + 200}})
  if (#game.players <= 1) then
    game.show_message_dialog{text = {"msg-intro"}}
  else
    player.print({"msg-intro"})
  end
  silo_script.gui_init(player)
end)

script.on_event(defines.events.on_player_respawned, function(event)
  local player = game.players[event.player_index]
  player.insert{name="pistol", count=1}
  player.insert{name="firearm-magazine", count=10}
end)

--------------------------------------------------------------------------------------
script.on_event(defines.events.on_gui_click, function(event)
	silo_script.on_gui_click(event)

	if event.element.name == "btn_ss" then
		local player = game.players[event.player_index]
		player.force.set_spawn_position(player.position,player.surface)
	end
	
	if event.element.name == "btn_cm" then
		local player = game.players[event.player_index]
		if market == nil then
			market = player.character.surface.create_entity{name = "market", position = player.character.surface.find_non_colliding_position("market", player.character.position, 10, 1)}
			market.destructible = false
		else
			market.teleport(player.character.surface.find_non_colliding_position("market", player.character.position, 10, 1))
		end
	end

	if event.element.name == "chk_hcsc" then
		update_gui(event.player_index)
	end
  
end)

script.on_init(function()
  global.version = version
  silo_script.init()
end)

script.on_event(defines.events.on_rocket_launched, function(event)
  silo_script.on_rocket_launched(event)
end)

script.on_configuration_changed(function(event)
  if global.version ~= version then
    global.version = version
  end
  silo_script.on_configuration_changed(event)
end)

silo_script.add_remote_interface()

--------------------------------------------------------------------------------------
function build_gui()
	local player
	
	for _, player in pairs(game.players) do
		if player.connected then
			if player.gui.top.hmf == nil then	
				player.gui.top.add(
				{
					type = "frame", 
					name = "hmf", 
					caption = "", 
					direction = "vertical"
				})
			end
			
			local gui_main = player.gui.top.hmf
			
			if gui_main.hcf == nil then	
				local gui_hcf = gui_main.add(
				{
					type = "flow", 
					name = "hcf", 
					direction = "horizontal"
				})
				gui_hcf.add(
				{
					type = "label", 
					name = "lbl_hcl", 
					caption = "htd tools" , 
					font_color = white
				})	
				gui_hcf.add(
				{
					type = "checkbox", 
					name = "chk_hcsc", 
					state = false
				})									
			end
		end
	end
end

--------------------------------------------------------------------------------------
function update_gui(player_index)
	local player = game.players[player_index]
	--This is assuming where this is being called the proper gui containers are made already
	local guif = player.gui.top.hmf.hcf
	if not guif.chk_hcsc.state then 
		if guif.btn_cm then guif.btn_cm.destroy() end
		if guif.btn_ss then guif.btn_ss.destroy() end
		if guif.lbl_cred then guif.lbl_cred.destroy() end
	else
		if player.admin or player.force.players[1] == player then
			guif.add({type = "button",caption = "Call Market",name = "btn_cm",style="button_style"})
			guif.add({type = "button",caption = "Set Spawn",name = "btn_ss",style="button_style"})	
		end
		guif.add({
					type = "label", 
					name = "lbl_cred", 
					caption = global.player_credits[player_index] , 
					font_color = white
				})
	end
end

--------------------------------------------------------------------------------------
function update_shop_items()

	if market ~= nil then
		
		if market.get_market_items() == nil then
			market.add_market_item{price={{"coin", 10}}, offer={type="give-item", item="copper-ore"}}
			market.add_market_item{price={{"copper-ore", 1}}, offer={type="give-item", item="coin", count=10}}
		end
		
	end

end

--------------------------------------------------------------------------------------
script.on_event(defines.events.on_entity_died,function(event)

--small-biter medium-biter big-biter behemoth-biter
--small-spitter medium-spitter big-spitter behemoth-spitter
--small-worm-turret medium-worm-turret big-worm-turret
--spitter-spawner biter-spawner
	game.print("something died: " .. event.entity.name)
	local force = event.force
	if force ~= nil then
		--game.print("force " .. force.name .. " did the killing.")
		if force.players ~= nil then
			for _, player in pairs(force.players) do
				if player.connected then			
					if global.player_credits[player.index] ~= nil then
						global.player_credits[player.index] = global.player_credits[player.index] + 5
					end
				end
			end
		end
	end

	--if cause ~= nil then
		--if cause.player ~= nil then
		--end
	--end
	
	
end)