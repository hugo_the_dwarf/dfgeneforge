global.force_credit --{} list of forces that have players

global.force_credit[force].small_unit_counter --list of currently killed small biters and spitters
global.force_credit[force].medium_unit_counter --list of currently killed medium biters and spitters
global.force_credit[force].big_unit_counter --list of currently killed big biters and spitters
global.force_credit[force].behemoth_unit_counter --list of currently killed behemoth biters and spitters
global.force_credit[force].deminish_countdown --once this hits 0 reduce counter by 1

local credit_earn_small = {unit_min = 1, unit_max = 20, turret = 50}
local credit_earn_medium = {unit_min = 10, unit_max = 50, turret = 150}
local credit_earn_big = {unit_min = 35, unit_max = 100, turret = 300}
local credit_earn_behemoth = {unit_min = 150, unit_max = 300, turret = 1} -- no behemoth turret worm

local small_unit_max_deminish = 30 --how many units to reach the min earn rate, kill this amount and it and the rest are min credit return rate
local medium_unit_max_deminish = 50
local big_unit_max_deminish = 100
local behemoth_unit_max_deminish = 100

local deminish_countdown_max = 3

local refresh_rate = 60 * 60 --1 min
--local refresh_deminish_rate = refresh_rate / 2


if game.tick % refresh_rate then

	--for force, force in pairs(global.force_credit)
		
		--if game.forces[force] ~= nil then
			--if game.forces[force].connectedplayers > 0 then
				
			--end
		--end
		
		
		--
		
	--end

	deminish_countdown_max = deminish_countdown_max - 1
	if deminish_countdown_max <= 0 then
		--for global.force_credit 
			--if force.connected player > 1 then
				--global.force_credit[force].small_unit_counter
				--global.force_credit[force].medium_unit_counter
				--global.force_credit[force].big_unit_counter
				--global.force_credit[force].behemoth_unit_counter
			--end
		--end
	end

end






























local function main()


--runtime stuff, not needed
local ticks_to_run = (60 * 60) * 2
local current_tick = 0

--already configured in runtime, not needed
local game = {}
game.forces = {}
game.forces["neutral"] = {}
game.forces["enemy"] = {}
game.forces["player"] = {}
game.forces["player2"] = {}
game.forces["player3"] = {}

game.forces["player"].players = {}
game.forces["player"].players[1] = {}
game.forces["player"].players[1].connected = true;
game.forces["player"].players[2] = {}
game.forces["player"].players[2].connected = false;
game.forces["player"].players[3] = {}
game.forces["player"].players[3].connected = true;

game.forces["player2"].players = {}
game.forces["player2"].players[1] = {}
game.forces["player2"].players[1].connected = false;
game.forces["player2"].players[2] = {}
game.forces["player2"].players[2].connected = true;

game.forces["player3"].players = {}
game.forces["player3"].players[1] = {}
game.forces["player3"].players[1].connected = false;

local global = {} -- this would already be set

--start of mod stuff
local checked_values = false

local credit_earn_small = {unit_min = 1, unit_max = 20, turret = 50}
local credit_earn_medium = {unit_min = 10, unit_max = 50, turret = 150}
local credit_earn_big = {unit_min = 35, unit_max = 100, turret = 300}
local credit_earn_behemoth = {unit_min = 150, unit_max = 300, turret = 500} -- no behemoth turret worm

local small_unit_max_deminish = 30 --how many units to reach the min earn rate, kill this amount and it and the rest are min credit return rate
local medium_unit_max_deminish = 50
local big_unit_max_deminish = 100
local behemoth_unit_max_deminish = 100

local deminish_countdown_max = 3

local refresh_rate = 60 * 60 --1 min

function add_to_force_counter(force,counter,counter_max)
  local c = global.force_credit[force].counters[counter]
  if c < counter_max then
    return c + 1
  else
    return c
  end

end

function reduce_force_counter(force,counter)
  local c = global.force_credit[force].counters[counter]
  if c > 0 then
    return c - 1
  else
    return c
  end

end

function setup_force_deminish(force)
    print(force)
    if global.force_credit[force] == nil then
        global.force_credit[force] = {}
        global.force_credit[force].counters = {}
        global.force_credit[force].counters["small_unit_counter"] = 0
        global.force_credit[force].counters["medium_unit_counter"] = 0
        global.force_credit[force].counters["big_unit_counter"] = 0
        global.force_credit[force].counters["behemoth_unit_counter"] = 0
        global.force_credit[force].deminish_countdown = deminish_countdown_max
    end
    
end

--tick group

while current_tick <= ticks_to_run do

    current_tick = current_tick + 1
    
    if checked_values == false then
      if global.force_credit == nil then global.force_credit = {} end
        for force, objects in pairs(game.forces) do
           
            if objects.players ~= nil then
               setup_force_deminish(force)
            end
            
        end
        print("-----       -----")
        print("----- Break -----")
        print("-----       -----")
        
        global.force_credit["player2"].counters["medium_unit_counter"] = 7
        global.force_credit["player"].counters["behemoth_unit_counter"] = 2
        
        checked_values = true 
    end
    
--    if current_tick == 3000 or current_tick == 4000 then
--        global.force_credit["player"].deminish_countdown = deminish_countdown_max
--    end
    
    --refresh timer
    if current_tick % refresh_rate then
        
        for force, force_objects in pairs(game.forces) do
           
            if force_objects.players ~= nil then
                local credit_force = global.force_credit[force]
                for id, player in ipairs(force_objects.players) do
                    
                    if player.connected == true then
                        
                        if global.force_credit[force].deminish_countdown > 0 then
                            
                            global.force_credit[force].deminish_countdown = global.force_credit[force].deminish_countdown - 1
                            print("force:" .. force .. "   player_id:" .. id .. "   dc:" .. global.force_credit[force].deminish_countdown .. "   ts:" .. os.time())
                        else
                            local will_have_reduced = 0
                            for counter, count in pairs(global.force_credit[force].counters) do
                              will_have_reduced = will_have_reduced + global.force_credit[force].counters[counter]
                              global.force_credit[force].counters[counter] = reduce_force_counter(force,counter)
                            end
                            if will_have_reduced ~= 0 then
                              global.force_credit[force].deminish_countdown = deminish_countdown_max
                            end
                        
                        end
                        --print(force .. " " .. id .. " " .. global.force_credit[force].deminish_countdown)
                        break
                    end
                
                end
                global.force_credit[force] = credit_force
            end
            
        end
        
    end
    --os.execute("ping - n 1 localhost > NUL") -- not needed just to emulate 1 second
end


--local tableA = {}
--tableA["t1"] = {}
--tableA["t1"].obj1 = 5
--tableA["t1"].obj2 = 51

--for key,value in pairs(tableA) do 
 --   for item, item_value in pairs(value) do
--        print(key .. ", " .. item .. ", " .. item_value)
    --end
   -- print(key .. ", " .. value.obj1) 
    --print(key .. ", " .. value.obj2) 
--end



end
main()
