--require "defines"
--require "util"

local pcClasses = 
{
	--ClassBody
	Engineer={
		name="Engineer",
		abbrName="Eng",
		subClasses=
		{
			Craftsman={
				name="Craftsman",
				abbrName="Crft",
				characterBonuses =
				{
					character_crafting_speed_modifier = 1.5
				},
				customBonuses=
				{
					freeCraftChance=20 --0 to 100 chance. Returns materials used.
				}
			},
			Roboticist={
				name="Roboticist",
				abbrName="Robo",
				characterBonuses=
				{
					character_maximum_following_robot_count_bonus = 9
				},
				customBonuses=
				{
					auraRange=2,
					auraRobots=true,
					robotRepairRate = 2.37521,
					robotEnergyRate = 95750,
					robotTimeToLive = 130 -- ticks, 60 is 1 second. so 1.5 seconds
					
				}
			},
			Technician={
				name="Technician",
				abbrName="Tech",
				characterBonuses=
				{
					character_build_distance_bonus = 4,
					character_reach_distance_bonus = 4,
					character_mining_speed_modifier = 1.5
				},
				customBonuses=
				{
					auraRange=5,
					machineRepairRate = 2.37521
				}
			}
		},
		characterBonuses=
		{
			character_inventory_slots_bonus = 10,
			character_build_distance_bonus = 3,
			character_reach_distance_bonus = 3,
			character_crafting_speed_modifier = 1.5
		},
		customBonuses=
		{
			hasAura=true,
			auraRange=5,
			auraMachines=true,
			machineRepairRate= 1.1578
		}
	},
	Soldier={
		name="Soldier",
		abbrName="Sldr",
		subClasses=
		{
			Scout={
				name="Scout",
				abbrName="Scot",
				characterBonuses=
				{
					character_running_speed_modifier = 0.15
				},
				customBonuses=
				{
					healRate=1.587
				}
			},
			Marine={
				name="Marine",
				abbrName="Mrin",
				characterBonuses=
				{
					character_health_bonus = 100
				},
				customBonuses=
				{
					healRate=2.2478
				}
			},
			Heavy={
				name="Heavy",
				abbrName="Hevy",
				characterBonuses=
				{
					character_health_bonus = 250,
					character_running_speed_modifier = -0.10
				},
				customBonuses=
				{
					healRate=3.2478
				}
			}
		},
		characterBonuses=
		{
			character_running_speed_modifier = 0.25,
			character_health_bonus = 100,
			character_crafting_speed_modifier = -0.5,
			character_mining_speed_modifier  = -0.5
		},
		customBonuses=
		{
			selfHeal=true,
			healRate=4.17826
		}
	},
	Driver={
		name="Driver",
		abbrName="Driv",
		subClasses=
		{
			--Transporter makes the vehicle use only 10% fuel per tick while keeping no loss in speed, also increases top speed
			Transporter={
				name="Transporter",
				abbrName="Trpt",
				characterBonuses=
				{
					character_inventory_slots_bonus = 50
				},
				vehicleBonuses=
				{
					effectivity_modifier = 9, --Base is 1
					consumption_modifier = -0.4, --Base is 1
					friction_modifier = -0.2 --Base is 1
				}
			},
			--Mechanics increase the rate of repair, but the consumption_rate is only reduced by 25% instead of 50%, but get a 25% boost to speed
			Mechanic={
				name="Mechanic",
				abbrName="Mech",
				customBonuses=
				{
					vehicleRepairRate=0.31643
				},
				vehicleBonuses=
				{
					effectivity_modifier  = 3, --Base is 1
					consumption_modifier = 0.25 --Base is 1
				}				
			},
			--Racers make the car go twice as fast, but consumption is left unchanged, they also repair at half the rate as normal, and can reach top speeds that other Classes cannot with any vehicle
			Racer={
				name="Racer",
				abbrName="Race",
				customBonuses=
				{
					vehicleRepairRate=-0.13413
				},
				vehicleBonuses=
				{
					effectivity_modifier  = 3, --Base is 1
					consumption_modifier = 0.5, --Base is 1
					friction_modifier = -0.35 --Base is 1
				}				
			}
		},
		customBonuses=
		{
			buffVehicles=true,
			vehicleRepairRate=0.27826
		},
		vehicleBonuses=
		{
			effectivity_modifier = 1, --Base is 1
			consumption_modifier = -0.5 --Base is 1
		}
	}
}

local pcAuraPlayers = {}
local pcSelfHealPlayers = {}
local pcDriverPlayers = {}

local pcPlayerClassValues = {} --1 is engineer, 2 is Soldier

local pcBuffedVehicles = {} --vehicleBonuses

local next = next

local function applyClassSettings(player_index, class, subClass)

	if pcPlayerClassValues[player_index] ~= nil then
		if pcAuraPlayers[player_index] ~= nil then
			pcAuraPlayers[player_index] = nil
		end
		if pcSelfHealPlayers[player_index] ~= nil then
			pcSelfHealPlayers[player_index] = nil
		end
		if pcDriverPlayers[player_index] ~= nil then
			pcDriverPlayers[player_index] = nil
		end
		
		if pcPlayerClassValues[player_index].oldCharacterValues ~= nil then
			local playerCharacter = game.players[player_index].character
			for customBonusIndex, customBonusValue in pairs(pcPlayerClassValues[player_index].oldCharacterValues) do
				playerCharacter[customBonusIndex] = customBonusValue
			end
		end		
		
		if pcPlayerClassValues[player_index].playerRealName ~= nil then
			game.players[player_index].name =  pcPlayerClassValues[player_index].playerRealName
		end
		pcPlayerClassValues[player_index] = nil
	end

	if pcPlayerClassValues[player_index] == nil then
		pcPlayerClassValues[player_index] = {}		
		if class then
			local pickedClass = pcClasses[class]
			
			pcPlayerClassValues[player_index].className = pickedClass.name
			if pcPlayerClassValues[player_index].playerRealName == nil then
				pcPlayerClassValues[player_index].playerRealName = game.players[player_index].name
			end
			local newPlayerName = pcPlayerClassValues[player_index].playerRealName .. ": " .. pickedClass.abbrName
			local playerCharacter = game.players[player_index].character
			
			if pickedClass.characterBonuses then
				pcPlayerClassValues[player_index].oldCharacterValues = {}				
				for characterBonusIndex, characterBonusValue in pairs(pickedClass.characterBonuses) do					
					pcPlayerClassValues[player_index].oldCharacterValues[characterBonusIndex] = playerCharacter[characterBonusIndex]
					playerCharacter[characterBonusIndex] = characterBonusValue					
				end
			end
			
			if pickedClass.customBonuses then
				pcPlayerClassValues[player_index].customBonuses = {}
				for customBonusIndex, customBonusValue in pairs(pickedClass.customBonuses) do
					if customBonusIndex == "hasAura" then
						pcAuraPlayers[player_index] = customBonusValue
					elseif customBonusIndex == "selfHeal" then
						pcSelfHealPlayers[player_index] = customBonusValue
					elseif customBonusIndex == "buffVehicles" then
						pcDriverPlayers[player_index] = customBonusValue
					else
						pcPlayerClassValues[player_index].customBonuses[customBonusIndex] = customBonusValue
					end
				end
			end
			
			if pickedClass.vehicleBonuses then
				pcPlayerClassValues[player_index].vehicleBonuses = {}
				for vehicleBonusIndex, vehicleBonusValue in pairs(pickedClass.vehicleBonuses) do
					pcPlayerClassValues[player_index].vehicleBonuses[vehicleBonusIndex] = vehicleBonusValue
				end
			end
			
			
			--if there is a subClass
			if subClass then
				local pickedSubClass = pickedClass.subClasses[subClass]
				newPlayerName = newPlayerName.."-"..pickedSubClass.abbrName
				
				
				if pickedSubClass.characterBonuses then
					if not pcPlayerClassValues[player_index].oldCharacterValues then
						pcPlayerClassValues[player_index].oldCharacterValues = {}		
					end
					for characterBonusIndex, characterBonusValue in pairs(pickedSubClass.characterBonuses) do	
						if not pcPlayerClassValues[player_index].oldCharacterValues[characterBonusIndex] then
							pcPlayerClassValues[player_index].oldCharacterValues[characterBonusIndex] = playerCharacter[characterBonusIndex]
							playerCharacter[characterBonusIndex] = characterBonusValue
						else
							playerCharacter[characterBonusIndex] = playerCharacter[characterBonusIndex] + characterBonusValue
						end
					end
				end
				
				if pickedSubClass.customBonuses then
					if not pcPlayerClassValues[player_index].customBonuses then
						pcPlayerClassValues[player_index].customBonuses = {}
					end
					for customBonusIndex, customBonusValue in pairs(pickedSubClass.customBonuses) do
						if customBonusIndex == "hasAura" then
							pcAuraPlayers[player_index] = customBonusValue
						elseif customBonusIndex == "selfHeal" then
							pcSelfHealPlayers[player_index] = customBonusValue
						elseif customBonusIndex == "buffVehicles" then
							pcDriverPlayers[player_index] = customBonusValue
						else
							if not pcPlayerClassValues[player_index].customBonuses[customBonusIndex] then
								pcPlayerClassValues[player_index].customBonuses[customBonusIndex] = customBonusValue
							else
								pcPlayerClassValues[player_index].customBonuses[customBonusIndex] = pcPlayerClassValues[player_index].customBonuses[customBonusIndex] + customBonusValue
							end
						end
					end
				end
				
				if pickedSubClass.vehicleBonuses then
					if not pcPlayerClassValues[player_index].vehicleBonuses then
						pcPlayerClassValues[player_index].vehicleBonuses = {}
					end
					for vehicleBonusIndex, vehicleBonusValue in pairs(pickedSubClass.vehicleBonuses) do
						if not pcPlayerClassValues[player_index].vehicleBonuses[vehicleBonusIndex] then
							pcPlayerClassValues[player_index].vehicleBonuses[vehicleBonusIndex] = vehicleBonusValue
						else
							pcPlayerClassValues[player_index].vehicleBonuses[vehicleBonusIndex] = global.pcPlayerClassValues[player_index].vehicleBonuses[vehicleBonusIndex] + vehicleBonusValue
						end
					end
				end
				
				
			end
			game.players[player_index].name = newPlayerName
			pcPlayerClassValues[player_index].newPlayerName = newPlayerName
			
		end		
	end
end

local function showGui(gui)
	if gui["pcFrameMain"] == nil then		
		gui.add{type="frame", name="pcFrameMain", direction="horizontal"}
		local mainFrame = gui["pcFrameMain"]
		if pcClasses then
			for f1_index, class in pairs(pcClasses) do
				local frameId = "pcTable"..class.name
				--mainFrame.add{type="label", name="pcLabel"..class.name, caption =class.name}
				mainFrame.add{type="table", name=frameId, column_count=1}				
				local classFrame = mainFrame[frameId]
				classFrame.add{type="label", name="pcLabel"..class.name, caption =class.name}
				if class.subClasses then					
					for f2_index, subClass in pairs(class.subClasses) do
						local buttonName = class.name .. "." .. subClass.name
						classFrame.add{type="button", name=buttonName, caption=subClass.name}
					end
				end
				
			end
		end
		--gui["pcFrameMain"].add{type="button", name="pcMainClassEngineer", caption ="Engineer"}
		--gui["pcFrameMain"].add{type="button", name="pcMainClassSoldier", caption ="Soldier"}
	end
end

local function hideGui(gui)
	if gui["pcFrameMain"] ~= nil then
		gui["pcFrameMain"].destroy()
	end
end

local function keyPress(Event)
	local gui = game.players[Event.player_index].gui.center
	--local index = Event.player_index
	--local player = game.players[index]
	if gui["pcFrameMain"] == nil then
		showGui(gui)
	else
		hideGui(gui)
	end
end

local function guiClick(event)
	local elem = event.element
	if elem.type == "button" then
		--game.print(elem.name)
		local parts = {}
		local partIndex = 1
		for i in string.gmatch(elem.name,"%a+") do
			parts[partIndex] = i
			partIndex = partIndex + 1
		end
		--game.print(parts[1] .. " " .. parts[2])
		--game.print(pcClasses[parts[1]].subClasses[parts[2]].abbrName)
		local index = event.player_index
		if parts[1] then
			applyClassSettings(index, parts[1], parts[2])
		end
	end
	
	
end

local function addTimeToLive(entity, amount)
	local maxTime = v.prototype.time_to_live
	local currentTime = v.time_to_live	
	currentTime = currentTime + amount
	if currentTime > maxTime then 
		currentTime = maxTime 
	end	
	v.time_to_live = currentTime
end

local function healEntity(entity, amount)
	if entity.health ~= nil then		
		entity.health = entity.health + amount
	end
end

local function addEnergy(entity,amount)
	if entity.prototype.max_energy ~= nil then
		local maxEnergy = entity.prototype.max_energy
		local currentEnergy = entity.energy
		currentEnergy = currentEnergy + amount
		if currentEnergy > maxEnergy then
			currentEnergy = maxEnergy
		end
		entity.energy = currentEnergy
	end
end

local function auraTickLogic()
	for index, boolean in ipairs(pcAuraPlayers) do
		local player = game.players[index]
		local force = player.force
		local surface = player.surface
		local loc = player.position
		
		local auraAffectsRobots = pcPlayerClassValues[index].customBonuses.auraRobots
		local auraRange = pcPlayerClassValues[index].customBonuses.auraRange
		local machineRepairRate = pcPlayerClassValues[index].customBonuses.machineRepairRate
		
		local ents = surface.find_entities_filtered
		{
			area = {
					{loc.x - auraRange, loc.y - auraRange},
					{loc.x + auraRange, loc.y + auraRange}
			},
			force = force
		}
		if ents ~= nil then
			for i, e in ipairs(ents) do
				if e.type ~= "player" then --don't heal players
					if e.type == "combat-robot" and auraAffectsRobots then
						healEntity(e, machineRepairRate + pcPlayerClassValues[index].customBonuses.robotRepairRate)
						addTimeToLive(e, pcPlayerClassValues[index].customBonuses.robotTimeToLive)
					elseif (e.type ==  "construction-robot" or e.type == "logistic-robot") and auraAffectsRobots then
						healEntity(e, machineRepairRate + pcPlayerClassValues[index].customBonuses.robotRepairRate)
						addEnergy(e, pcPlayerClassValues[index].customBonuses.robotEnergyRate)
					elseif e.type ~= "car" and e.type ~= "locomotive" then
						healEntity(e, machineRepairRate)
					end
				end
			end
		end
	end
end

local function selfHealTickLogic()
	for index, boolean in ipairs(pcSelfHealPlayers) do
		healEntity(game.players[index].character, pcPlayerClassValues[index].customBonuses.healRate)
	end
end

local function driverRepairTickLogic()
	for index, boolean in ipairs(pcDriverPlayers) do
		local player = game.players[index]
		if player.vehicle then
			local vehicle = player.vehicle
			if vehicle.get_driver().player.index == index then
				healEntity(vehicle, pcPlayerClassValues[index].customBonuses.vehicleRepairRate)
			elseif vehicle.get_passenger().index == index then
				healEntity(vehicle, pcPlayerClassValues[index].customBonuses.vehicleRepairRate / 2)
			end
		end
	end
end

local function setVehicleBuffs(index, entity, resetBuffs)

	local oldValuesSaved = true
	if not pcBuffedVehicles[index].oldVehicleBonuses then
		oldValuesSaved = false
		pcBuffedVehicles[index].oldVehicleBonuses = {}
	end
	
	if resetBuffs == false then
		for vehicleBonusIndex, vehicleBonusValue in pairs(pcPlayerClassValues[index].vehicleBonuses) do		
			if oldValuesSaved == false then
				pcBuffedVehicles[index].oldVehicleBonuses[vehicleBonusIndex] = entity[vehicleBonusIndex]
			end
			entity[vehicleBonusIndex] = entity[vehicleBonusIndex] + vehicleBonusValue
		end
	else
		if pcBuffedVehicles[index].oldVehicleBonuses then
			for vehicleBonusIndex, vehicleBonusValue in pairs(pcBuffedVehicles[index].oldVehicleBonuses) do				
				entity[vehicleBonusIndex] = vehicleBonusValue
			end
		else --defaults of base 1 if there are no old values to pick from
			for vehicleBonusIndex, vehicleBonusValue in pairs(pcPlayerClassValues[index].vehicleBonuses) do	
				entity[vehicleBonusIndex] = 1
			end
		end
	end

end

local function playerDrivingStateChangedLogic(event)
	local index = event.player_index
	
	if next(pcDriverPlayers) ~= nil then
	
		if pcDriverPlayers[index] then
			if event.entity ~= nil then
				local vehicle = event.entity
				if vehicle.get_driver() ~= nil then
					if vehicle.get_driver().player ~= nil then
						if vehicle.get_driver().player.index == index then --Only the Driver applies these buffs
							if pcBuffedVehicles[index] == nil then
								pcBuffedVehicles[index] = {}
								pcBuffedVehicles[index].entity = vehicle					
								setVehicleBuffs(index, vehicle, false)
							end
						end
					end
				else
					if pcBuffedVehicles[index] ~= nil then
						if pcBuffedVehicles[index].entity ~= nil then
							if pcBuffedVehicles[index].entity.valid then
								setVehicleBuffs(index, pcBuffedVehicles[index].entity, true)
								pcBuffedVehicles[index] = nil --Delete the record
							end
						end
					end
				end
			else
				if pcBuffedVehicles[index] then
					pcBuffedVehicles[index] = nil --Delete the record
				end
			end
		end
	else --If there are no drivers, but there are buffed vehicles
		if next(pcBuffedVehicles) then
			for vehicleIndex, vehicleBuffedTable in ipairs(pcBuffedVehicles) do
				if pcBuffedVehicles[index].entity ~= nil then
					if pcBuffedVehicles[index].entity.valid then
						setVehicleBuffs(index, pcBuffedVehicles[vehicleIndex].entity, true)						
					end
				end
				pcBuffedVehicles[vehicleIndex] = nil --Delete the record
			end
		end
	end --next
end --func

local function drivingStateChangedTestLogic(event)
	
	local outputString = "Output: " .. " :Player Index " .. event.player_index
	
	if event.entity ~= nil then
		outputString = outputString .. " :Entity Name " .. event.entity.name
		if event.entity.get_driver() ~= nil then
			outputString = outputString .. " :Driver yes"
			if event.entity.get_driver().player ~= nil then
				outputString = outputString .. " :Driver Index " .. event.entity.get_driver().player.index
			end
		end
	else
		outputString = outputString .. " :Entity is Nil"
	end
	

	
	game.print(outputString)

end

local function pcTickLogic(event)
	
	if next(pcAuraPlayers) ~= nil then
		auraTickLogic()
	end
	
	if next(pcSelfHealPlayers) ~= nil then
		selfHealTickLogic()
	end
	
	if next(pcDriverPlayers) ~= nil then
		driverRepairTickLogic()
	end

end

local function mapGlobalsToLocals()
	pcAuraPlayers = global.pcAuraPlayers
	pcSelfHealPlayers = global.pcSelfHealPlayers
	pcDriverPlayers = global.pcDriverPlayers

	pcPlayerClassValues = global.pcPlayerClassValues

	pcBuffedVehicles = global.pcBuffedVehicles --vehicleBonuses
end

local function onInit()
	global.pcAuraPlayers = {}
	global.pcSelfHealPlayers = {}
	global.pcDriverPlayers = {}

	global.pcPlayerClassValues = {} 

	global.pcBuffedVehicles = {} --vehicleBonuses
	
	mapGlobalsToLocals()
end

local function onLoad()
	mapGlobalsToLocals()
end

script.on_init(onInit)
script.on_load(onLoad)

script.on_event
(
	"pc-class-select-key",
	keyPress
)

script.on_event
(
	defines.events.on_gui_click,
	guiClick
)

script.on_event
(
	defines.events.on_player_driving_changed_state,
	--drivingStateChangedTestLogic
	playerDrivingStateChangedLogic
)

script.on_nth_tick(60, pcTickLogic)