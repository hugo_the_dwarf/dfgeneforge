tissue_template_htd_tech

[OBJECT:TISSUE_TEMPLATE]
	
[TISSUE_TEMPLATE:HTD_TECH_STEEL_TEMPLATE]
	[TISSUE_NAME:steel:NP]
	[STRUCTURAL][SPLINTABLE]
	[TISSUE_MATERIAL:INORGANIC:STEEL]
	[RELATIVE_THICKNESS:3]
	[HEALING_RATE:200]
	[CONNECTS]
	[TISSUE_SHAPE:LAYER]

[TISSUE_TEMPLATE:HTD_TECH_IRON_TEMPLATE]
	[TISSUE_NAME:iron:NP]
	[STRUCTURAL][SPLINTABLE][SCARS]
	[TISSUE_MATERIAL:INORGANIC:IRON]
	[RELATIVE_THICKNESS:2]
	[HEALING_RATE:200]
	[CONNECTS]
	[TISSUE_SHAPE:LAYER]
	
[TISSUE_TEMPLATE:HTD_TECH_HOSE_TEMPLATE]
	[TISSUE_NAME:hose:hoses]
	[FUNCTIONAL]
	[TISSUE_MATERIAL:INORGANIC:BRASS]
	[RELATIVE_THICKNESS:1]
	[HEALING_RATE:200]
	[CONNECTS]
	[SUBORDINATE_TO_TISSUE:PISTON]
	[TISSUE_SHAPE:LAYER]
	
[TISSUE_TEMPLATE:HTD_TECH_PISTON_TEMPLATE]
	[TISSUE_NAME:piston:pistons]
	[MUSCULAR]
	[TISSUE_MATERIAL:INORGANIC:COPPER]
	[RELATIVE_THICKNESS:1]
	[HEALING_RATE:200]
	[TISSUE_SHAPE:LAYER]
	
[TISSUE_TEMPLATE:HTD_TECH_OIL_TEMPLATE]
	[TISSUE_NAME:oil:NP]
	[TISSUE_MATERIAL:CREATURE_MAT:HTD_TECH_MATS:OIL]
	[RELATIVE_THICKNESS:1]
	[TISSUE_MAT_STATE:LIQUID]
	[HEALING_RATE:200]
	[SUBORDINATE_TO_TISSUE:HOSE]
	[TISSUE_SHAPE:LAYER]
	
[TISSUE_TEMPLATE:HTD_TECH_GAS_TEMPLATE]
	[TISSUE_NAME:gasoline:NP]
	[TISSUE_MATERIAL:CREATURE_MAT:HTD_TECH_MATS:GASOLINE]
	[RELATIVE_THICKNESS:100]
	[TISSUE_MAT_STATE:LIQUID]
	[FUNCTIONAL]
	[TISSUE_LEAKS]
	[TISSUE_SHAPE:LAYER]