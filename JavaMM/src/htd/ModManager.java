package htd;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import IO.LineReader;

public class ModManager {

	public static String objectBuffer = "";
	public static boolean objectStarted = false;

	public static void main(String[] args) {
		// String myPath = System.getProperty("user.dir");
		// String rawPath = ".." + File.separator + "raw";
		// String objectPath = rawPath + File.separator + "objects";
		File dir = new File(Constants.objectPath);
		File[] listOfFiles = dir.listFiles();
		BufferedReader br = null;
		BufferedWriter bw = null;

		String filePrefix = "inorganic_stone_layer";
		String objectPrefix = "[INORGANIC:";

		Template tem = TemplateManager.getTemplate("quarry_dig_stone");

		try {
			String output = "";
			for (File f : listOfFiles) {
				if (f.getName().startsWith(filePrefix)) {
					br = new BufferedReader(new FileReader(Constants.objectPath + File.separator + f.getName()));

					String line;

					while ((line = br.readLine()) != null) {
						if (line.startsWith(objectPrefix)) {
							tem.generateResultFromArgs(
									new String[] { LineReader.getObjectIdFromLine(line, objectPrefix), "TEST" });
							addToBuffer(LineReader.getObjectIdFromLine(line, objectPrefix), true);
						}
					}

					br.close();
				}
				if (tem.getResult() != "") {
					bw = new BufferedWriter(new FileWriter(Constants.templatePath + File.separator + "template_result_" + f.getName()));
					bw.write(tem.getResult());
					bw.close();
					tem.reset();
				}
			}
			writeBuffer();
		} catch (FileNotFoundException e) {
			// if(br != null)
			// br.close();
		} catch (IOException e) {
			// if(br != null)
			// br.close();
		}

		System.out.println(tem.getResult());
	}

	public static String startObject(String line, String tokenPrefix, int tokenPrefixLength, char tokenEnd) {
		String output;
		output = line.substring(line.indexOf(tokenPrefix) + tokenPrefixLength,
				line.substring(line.indexOf(tokenPrefix)).indexOf(tokenEnd));
		objectStarted = true;
		addToBuffer(output, true);
		return output;
	}

	public static void addToBuffer(String text, boolean newline) {
		objectBuffer += text + (newline ? "\n" : "");
	}

	public static void writeBuffer() {
		System.out.println(objectBuffer);
		objectBuffer = "";
		objectStarted = false;
	}
}
