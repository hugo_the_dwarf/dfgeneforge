package htd;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Template {

	private String templateName = "";
	private String templateBase = "";
	private String constructedResult = "";
	private String[] argsToReplace;

	public Template(String templateName) {
		this.templateName = templateName;
	}

	public void load() {
		BufferedReader br = null;
		try {
			String file = Constants.templatePath + File.separator + templateName;
			if(!templateName.contains(".txt"))
				file += ".txt";
			br = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = br.readLine()) != null) {
				templateBase += line + System.lineSeparator();
			}
		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public void generateResultFromArgs(String[] args) {
		argsToReplace = new String[args.length];
		for (int i = 0; i < argsToReplace.length; i++) {
			argsToReplace[i] = Constants.TEMPLATE_ARG_BASE + (i + 1);
		}

		String tempResult = templateBase;
		for (int i = 0; i < argsToReplace.length; i++) {
			tempResult = tempResult.replace("l" + argsToReplace[i], args[i].toLowerCase().replace("_", " "));
			tempResult = tempResult.replace(argsToReplace[i], args[i]);
		}
		constructedResult += tempResult + System.lineSeparator();
	}

	public String getResult() {
		return constructedResult;
	}
	
	public void reset()
	{
		constructedResult = "";
	}

}
