package htd;

public class TemplateManager {
	
	public static Template getTemplate(String templateName)
	{
		Template t = new Template(templateName);
		t.load();
		return t;
	}

}
