package htd;
import java.io.File;

public class Constants {

	//public static String myPath = System.getProperty("user.dir");
	public static String rawPath = ".." + File.separator + "raw";
	public static String objectPath = rawPath + File.separator + "objects";
	public static String hmlPath = ".." + File.separator + "hml";
	public static String templatePath = hmlPath + File.separator + "templates";
	public static String tokenMapPath = hmlPath + File.separator + "tokenMaps";
	
	public static final char TOKEN_START = '[';
	public static final char TOKEN_ARG_SEPARATER = ':';
	public static final char TOKEN_END = ']';
	
	public static final String TEMPLATE_ARG_BASE = "arg";
	
}
