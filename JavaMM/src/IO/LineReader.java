package IO;

import htd.Constants;

public class LineReader {

	public static String getObjectIdFromLine(String line, String prefix)
	{
		String output;
		int prefixLength = prefix.length();
		output = line.substring(line.indexOf(prefix) + prefixLength, line.substring(line.indexOf(prefix)).indexOf(Constants.TOKEN_END));
		return output;
	}
	
}
