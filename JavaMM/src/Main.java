import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Main extends JFrame implements ActionListener {

	JLabel answer = new JLabel("");
	JPanel pane = new JPanel();
	JButton pressme = new JButton("Press Me");

	Main() // the frame constructor method
	{
		super("My Simple Frame");
		setBounds(100, 100, 300, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container con = this.getContentPane(); // inherit main frame
		con.add(pane); // add the panel to frame
		pressme.setMnemonic('P'); // associate hotkey to button
		pressme.addActionListener(this); // register button listener
		pane.add(answer);
		pane.add(pressme);
		pressme.requestFocus();
		setVisible(true); // display this frame
	}

	public void actionPerformed(ActionEvent event) {
		Object source = event.getSource();
		if (source == pressme) {
			answer.setText("Button pressed!");
			JOptionPane.showMessageDialog(null, "I hear you!", "Message Dialog", JOptionPane.PLAIN_MESSAGE);
			setVisible(true); // show something
		}
	}

	public static void main(String[] args) {
		new Main();
	}

	//https://www.codeproject.com/articles/33536/an-introduction-to-java-gui-programming
}
