language_HUMAN_NORSE

[OBJECT:LANGUAGE]

[TRANSLATION:HUMAN_NORSE]
	[T_WORD:ABILITY:kunnsta]
	[T_WORD:ABODE:hamr]
	[T_WORD:ACT:breyta]
	[T_WORD:AFTER:sidan]
	[T_WORD:AGE:aevi]
	[T_WORD:ALE:mungat]
	[T_WORD:ANCIENT:forn]
	[T_WORD:ANGER:reidi]
	[T_WORD:ANIMAL:dyr]
	[T_WORD:ARCH:kengr]
	[T_WORD:ARM:handleggr]
	[T_WORD:ARMOR:herklaedi]
	[T_WORD:ARROW:or]
	[T_WORD:ARTIFICE:vel]
	[T_WORD:ASH:askr]
	[T_WORD:AUTHORITY:mannaforrad]
	[T_WORD:AUTUMN:haust]
	[T_WORD:AVENGE:reka]
	[T_WORD:AXE:ox]
	[T_WORD:BACK:aptr]
	[T_WORD:BAD:illr]
	[T_WORD:BALD:kollottr]
	[T_WORD:BAKE:baka]
	[T_WORD:BALL:knottr]
	[T_WORD:BANNER:merki]
	[T_WORD:BAR:spolr]
	[T_WORD:BARN:hlada]
	[T_WORD:BATH:bepa]
	[T_WORD:BATTLE:bardagi]
	[T_WORD:BEAK:gron]
	[T_WORD:BEAST:dyr]
	[T_WORD:BEAR:bjorn]
	[T_WORD:BEAR_VERB:bera]
	[T_WORD:BEE:by]
	[T_WORD:BEER:bjorr]
	[T_WORD:BEGINNING:fyrsta]
	[T_WORD:BELIEF:atrunadr]
	[T_WORD:BERSERK:berserkr]
	[T_WORD:BIG:mikill]
	[T_WORD:BIND:binda]
	[T_WORD:BIRD:fugl]
	[T_WORD:BLACK:blar]
	[T_WORD:BLADE:brandr]
	[T_WORD:BLAZE:loga]
	[T_WORD:BLIND:blindr]
	[T_WORD:BLOOD:blod]
	[T_WORD:BLOODY:blodugr]
	[T_WORD:BLUE:blar]
	[T_WORD:BOAT:batr]
	[T_WORD:BOG:fen]
	[T_WORD:BOIL:kaun]
	[T_WORD:BOIL_V:sjoda]
	[T_WORD:BOLDLY:djarfliga]
	[T_WORD:BONE:bein]
	[T_WORD:BOOK:bok]
	[T_WORD:BORDER:skaut]
	[T_WORD:BOW:almr]
	[T_WORD:BOW_VERB:luta]
	[T_WORD:BRAIN:heili]
	[T_WORD:BRAVE:bitr]
	[T_WORD:BREAD:braud]
	[T_WORD:BREAK:bresta]
	[T_WORD:BREAST:brjost]
	[T_WORD:BREATH:orindi]
	[T_WORD:BRIDE:brudr]
	[T_WORD:BRIDLE:beizl]
	[T_WORD:BRIGHT:bjartr]
	[T_WORD:BROTHER:brodir]
	[T_WORD:BUCK:bokki]
	[T_WORD:BUCKET:bytta]
	[T_WORD:BUNCH:kongull]
	[T_WORD:BURN:brenna]
	[T_WORD:BURY:heygja]
	[T_WORD:BUSINESS:sysla]
	[T_WORD:BUTTON:kneppa]
	[T_WORD:CAIRN:varda�]
	[T_WORD:CALF:kalfr]
	[T_WORD:CALL:heita]
	[T_WORD:CALM:logn]
	[T_WORD:CAPTURE:handtaka]
	[T_WORD:CARE:forsja]
	[T_WORD:CAST:gjota]
	[T_WORD:CASTLE:kastali]
	[T_WORD:CAT:kottr]
	[T_WORD:CAVE:hellir]
	[T_WORD:CELEBRATION:agaeti]
	[T_WORD:CHAMPION:kappi]
	[T_WORD:CHANGE:skipan]
	[T_WORD:CHARACTER:hagr]
	[T_WORD:CHARGE:sok]
	[T_WORD:CHARM:formali]
	[T_WORD:CHILD:barn]
	[T_WORD:CHILDHOOD:barnaeska]
	[T_WORD:CHOICE:kostr]
	[T_WORD:CHURCH:kirkja]
	[T_WORD:CHURL:karl]
	[T_WORD:CIRCLE:hringr]
	[T_WORD:CLASH:gnyr]
	[T_WORD:CLASP:spenna]
	[T_WORD:CLEAR:audsynn]
	[T_WORD:CLEARING:rjodr]
	[T_WORD:CLEAVE:kljufa]
	[T_WORD:CLIFF:bjarg]
	[T_WORD:CLOAK:skikkja]
	[T_WORD:CLOUD:sky]
	[T_WORD:CLUSTER:kongull]
	[T_WORD:COALS:kol]
	[T_WORD:COIL:baugr]
	[T_WORD:COLD:kaldr]
	[T_WORD:COLOR:litr]
	[T_WORD:COMFORT:huggan]
	[T_WORD:CONVERSE:tala]
	[T_WORD:COOK:matsveinn]
	[T_WORD:COPPER:eir]
	[T_WORD:COVER:hlif]
	[T_WORD:COWARDLY:argr]
	[T_WORD:CRAG:bjarg]
	[T_WORD:CRAVE:krefja]
	[T_WORD:CREEP:krjupa]
	[T_WORD:CROSS_NOUN:kross]
	[T_WORD:CROWD:fjolmenni]
	[T_WORD:CRUSADERS:Jorsalafarar]
	[T_WORD:CRY:hlakka]
	[T_WORD:CURSES:feiknastafir]
	[T_WORD:CUT:bita]
	[T_WORD:DALE:dalr]
	[T_WORD:DANCE:danzleikr]
	[T_WORD:DARK:blakkr]
	[T_WORD:DARKNESS:myrkr]
	[T_WORD:DAUNTLESS:hraustligr]
	[T_WORD:DAWN:elding]
	[T_WORD:DAY:dagr]
	[T_WORD:DEAD:daudr]
	[T_WORD:DEAR:dyrr]
	[T_WORD:DEATH:andlat]
	[T_WORD:DEBT:skuld]
	[T_WORD:DECEIT:flaerd]
	[T_WORD:DEEP:djupr]
	[T_WORD:DEFEND:vardi]
	[T_WORD:DELAY:dvol]
	[T_WORD:DESPAIR:aedra]
	[T_WORD:DESTROYER:farbjodr]
	[T_WORD:DIRTY:okraesilegr]
	[T_WORD:DITCH:grof]
	[T_WORD:DOG:hundr]
	[T_WORD:DOOMED:feigr]
	[T_WORD:DOOR:dyrr]
	[T_WORD:DRAWL:ogra]
	[T_WORD:DREGS:orval]
	[T_WORD:DRINKER:drykkjumadr]
	[T_WORD:DRIP:drjupa]
	[T_WORD:DROOP:drupa]
	[T_WORD:DRY:burr]
	[T_WORD:DUTY:knod]
	[T_WORD:DWARF:dvergr]

	[T_WORD:EAGLE:ari]
	[T_WORD:EARLY:snemt]
	[T_WORD:EASTWARD:austr]
	[T_WORD:EAT:eta]
	[T_WORD:EDGE:jadarr]
	[T_WORD:EGG:egg]
	[T_WORD:ELDER:ellri]
	[T_WORD:ENEMY:ovinr]
	[T_WORD:EVENING:aptann]
	[T_WORD:EVIL:illr]
	[T_WORD:EYE:auga]

	[T_WORD:FACE:andlit]
	[T_WORD:FALL:fall]
	[T_WORD:FALSE:flar]
	[T_WORD:FAMILY:aett]
	[T_WORD:FARM:bu]
	[T_WORD:FAST:fast]
	[T_WORD:FAT:feitr]
	[T_WORD:FATHER:fadir]
	[T_WORD:FEAR:aedra]
	[T_WORD:FEAST:gildi]
	[T_WORD:FETTER:band]
	[T_WORD:FIELD:akr]
	[T_WORD:FIERCE:grimmr]
	[T_WORD:FILTH:saurr]
	[T_WORD:FIND:finna]
	[T_WORD:FINGER:fingr]
	[T_WORD:FIRE:aldrnari]
	[T_WORD:FIRST:adr]
	[T_WORD:FISH_ANIMAL:fiskr]
	[T_WORD:FISH_VERB:fiskr]
	[T_WORD:FLAME:hiti]
	[T_WORD:FLEE:flyja]
	[T_WORD:FLESH:slatr]
	[T_WORD:FLIGHT:flaugun]
	[T_WORD:FLOOD:flod]
	[T_WORD:FLOUR:mjol]
	[T_WORD:FLY_VERB:fljuga]
	[T_WORD:FOG:boka]
	[T_WORD:FOOLISH:heimskr]
	[T_WORD:FOOT:fotr]
	[T_WORD:FOREST:mork]
	[T_WORD:FREE:frjals]
	[T_WORD:FREEZE:frjosa]
	[T_WORD:FROST:frost]
	[T_WORD:FROST-GIANT:hrimburs]
	[T_WORD:FUR:skinn]
	[T_WORD:FURY:modr]

	[T_WORD:GALLANT:vaskr]
	[T_WORD:GAME:leikr]
	[T_WORD:GARMENT:klaedi]
	[T_WORD:GAZE:sjon]
	[T_WORD:GHOST:draugr]
	[T_WORD:GIANT:jotunn]
	[T_WORD:GIFT:gjof]
	[T_WORD:GIRDLE:fetill]
	[T_WORD:GLACIER:jokull]
	[T_WORD:GLAD:gladr]
	[T_WORD:GLANCE:tillit]
	[T_WORD:GLORIOUS:maerr]
	[T_WORD:GLIDE:skrida]
	[T_WORD:GLITTER:glita]
	[T_WORD:GLORY:agaeti]
	[T_WORD:GLOVE:hanzki]
	[T_WORD:GLUM:ogladr]
	[T_WORD:GOAT:hafr]
	[T_WORD:GOD:ass]
	[T_WORD:GOLD:marglod]
	[T_WORD:GORGE:gil]
	[T_WORD:GRAIN:korn]
	[T_WORD:GRANT:unna]
	[T_WORD:GRAPE:vinber]
	[T_WORD:GRASS:gras]
	[T_WORD:GRAVE:haugr]
	[T_WORD:GREAT:mikill]
	[T_WORD:GREED:agirni]
	[T_WORD:GRIEF:bol]
	[T_WORD:GRIM:grimmr]
	[T_WORD:GRIN:glotta]
	[T_WORD:GRIP:gripa]
	[T_WORD:GROAN:stynr]
	[T_WORD:GROW:broask]
	[T_WORD:GROWTH:avoxtr]
	[T_WORD:GUARD:vordr]
	[T_WORD:GUILD:gildi]

	[T_WORD:HAIR:har]
	[T_WORD:HAIRY:lodinn]
	[T_WORD:HAMMER:hamarr]
	[T_WORD:HAND:hond]
	[T_WORD:HANDLE:skapt]
	[T_WORD:HARM:vansi]
	[T_WORD:HATEFUL:leidr]
	[T_WORD:HAUL:draga]
	[T_WORD:HAWK:haukr]
	[T_WORD:HEAD:hofud]
	[T_WORD:HEART:hjarta]
	[T_WORD:HEAT:bruni]
	[T_WORD:HEATHEN:heidinn]
	[T_WORD:HELL:hel]
	[T_WORD:HELM:styri]
	[T_WORD:HERO:halr]
	[T_WORD:HEW:hoggva]
	[T_WORD:HIDE:hud]
	[T_WORD:HIDEOUS:illiligr]
	[T_WORD:HIGH:har]
	[T_WORD:HILL:fell]
	[T_WORD:HILT:hjolt]
	[T_WORD:HOLE:grof]
	[T_WORD:HOME:heim]
	[T_WORD:HOOD:hofddukr]
	[T_WORD:HOOK:krokr]
	[T_WORD:HOP_VERB:skjota]
	[T_WORD:HORN:horn]
	[T_WORD:HORSE:hestr]
	[T_WORD:HOUND:hundr]
	[T_WORD:HOWL:yla]
	[T_WORD:HUGE:storr]
	[T_WORD:HUMAN:menskr]
	[T_WORD:HUNGER:gradr]

	[T_WORD:ICE:iss]
	[T_WORD:ILLNESS:sott]
	[T_WORD:IMPURE:ohreinn]
	[T_WORD:INCITE:etja]
	[T_WORD:IRON:jarn]

	[T_WORD:JAW:kjoptr]
	[T_WORD:JUDGMENT:domr]

	[T_WORD:KEEPING:hirzla]
	[T_WORD:KEY:lykill]
	[T_WORD:KINDLY:linr]
	[T_WORD:KILL:drepa]
	[T_WORD:KISS:kyssa]
	[T_WORD:KING:jofurr]
	[T_WORD:KNEAD:knoda]
	[T_WORD:KNIFE:sax]
	[T_WORD:KNOT:knutr]

	[T_WORD:LACE:bvengr]
	[T_WORD:LAKE:vatn]
	[T_WORD:LAST:sidastr]
	[T_WORD:LAW:log]
	[T_WORD:LEADER:folkhagi]
	[T_WORD:LEAF:laufsblad]
	[T_WORD:LETTER:bref]
	[T_WORD:LIE:lygi]
	[T_WORD:LIGHT:ljos]
	[T_WORD:LIP:gron]
	[T_WORD:LOCK:laesa]
	[T_WORD:LONG:langr]
	[T_WORD:LORD:herra]
	[T_WORD:LOSS:afrod]
	[T_WORD:LOUSE:lus]
	[T_WORD:LOVE:ast]
	[T_WORD:LOWER:nedar]
	[T_WORD:LUCK:gaefa]

	[T_WORD:MAD:aerr]
	[T_WORD:MAGIC:frodleikr]
	[T_WORD:MAN:holdr]
	[T_WORD:MANY:margr]
	[T_WORD:MARTYRDOM:martirium]
	[T_WORD:MEAD:mjodr]
	[T_WORD:MEADOW:eng]
	[T_WORD:MEAL_DOM:matr]
	[T_WORD:MEAL_GROUND:mjol]
	[T_WORD:MEAT:slatr]
	[T_WORD:MESSAGE:ord]
	[T_WORD:MIDSUMMER:midsumar]
	[T_WORD:MIGHTY:brottigr]
	[T_WORD:MIND:ged]
	[T_WORD:MISERABLE:vesall]
	[T_WORD:MIST:boka]
	[T_WORD:MOON:tungl]
	[T_WORD:MOTHER:modir]
	[T_WORD:MOUSE:mus]
	[T_WORD:MOUTH:mudr]
	[T_WORD:MUD:efja]
	[T_WORD:MURDER:mordvig]
	[T_WORD:MYSTERIES:run]

	[T_WORD:NAIL:nagl]
	[T_WORD:NASTY:obokkulig]
	[T_WORD:NATURE:lund]
	[T_WORD:NIGHT:nott]
	[T_WORD:NOBLE:godr]
	[T_WORD:NORTHWARDS:nordr]
	[T_WORD:NOSE:nef]

	[T_WORD:OAK:eik]
	[T_WORD:OARSMAN:haseti]
	[T_WORD:OATH:eidr]
	[T_WORD:OLD:aldinn]
	[T_WORD:ONE:ein]
	[T_WORD:OPEN:hniga]
	[T_WORD:OX:uxi]

	[T_WORD:PACE:fet]
	[T_WORD:PAGE:skosveinn]
	[T_WORD:PAIN:mein]
	[T_WORD:PALE:folr]
	[T_WORD:PALM:lofi]
	[T_WORD:PEACE:fridr]
	[T_WORD:PHANTOM:skripi]
	[T_WORD:PIT:grof]
	[T_WORD:PITCH:tjalda]
	[T_WORD:PLAGUE:mein]
	[T_WORD:POISON:eitr]
	[T_WORD:POWER:afli]
	[T_WORD:PRIDE:ofsi]
	[T_WORD:PRIEST:prestr]
	[T_WORD:PROPHECY:spa]
	[T_WORD:PROPHET:spakona]
	
	[T_WORD:QUEEN:drottning]
	[T_WORD:QUICK:skjotr

	
	[T_WORD:RACE_GROUP:ras]
	[T_WORD:RAG:flik]
	[T_WORD:RAGE:geisa]
	[T_WORD:RAKE:raka]
	[T_WORD:RAVEN:hrafn]
	[T_WORD:RED:rjodr]
	[T_WORD:REIN:taumr]
	[T_WORD:REND:slita]
	[T_WORD:RIDING:reid]
	[T_WORD:RIM:bord]
	[T_WORD:RING_OBJECT:hringr]
	[T_WORD:RIP:rifa]
	[T_WORD:RISE:risa]
	[T_WORD:RIVER:a]
	[T_WORD:ROCK:bjarg]
	[T_WORD:ROOM:rum]
	[T_WORD:ROOT:rot]
	[T_WORD:ROTTEN:fuinn]
	[T_WORD:RULER:hofdingi]

	[T_WORD:SCABBARD:slidir]
	[T_WORD:SCRATCH:klora]
	[T_WORD:SCREAM:gala]
	[T_WORD:SEA:aegir]
	[T_WORD:SEASON:arangr]
	[T_WORD:SECRECY:laun]
	[T_WORD:SEIZE:hondla]
	[T_WORD:SERPENT:ormr]
	[T_WORD:SERVANT:skosveinn]
	[T_WORD:SHELTER:hlif]
	[T_WORD:SHIELD:skjoldr]
	[T_WORD:SHIP:eik]
	[T_WORD:SHOOT:skjota]
	[T_WORD:SICKNESS:kvilla]
	[T_WORD:SILENCE:hljod]
	[T_WORD:SILVER:silfr]
	[T_WORD:SISTER:nipt]
	[T_WORD:SKULL:hauss]
	[T_WORD:SKY:lopt]
	[T_WORD:SLAUGHTER:mannfall]
	[T_WORD:SLAVE:braell]
	[T_WORD:SLAYER:banamadr]
	[T_WORD:SMILE:hlaeja]
	[T_WORD:SMITHY:smidja]
	[T_WORD:SMOKE:eimi]
	[T_WORD:SMOOTH:slettr]
	[T_WORD:SORROW:sut]
	[T_WORD:SOUL:atho]
	[T_WORD:SOUND:ond]
	[T_WORD:SOUTHWARD:ofan]
	[T_WORD:SPEAR:darradr]
	[T_WORD:SPELL:seidr]
	[T_WORD:SPIRIT:hugr]
	[T_WORD:SPIT:eyrr]
	[T_WORD:SPOIL:spilla]
	[T_WORD:SPRING_NOUN:kelda]
	[T_WORD:SPRING_SEASON:var]
	[T_WORD:SPRING_VERB:hrjota]
	[T_WORD:STAB:stinga]
	[T_WORD:STAFF:stafr]
	[T_WORD:STAND:standa]
	[T_WORD:STAR:stjarna]
	[T_WORD:STEED:goti]
	[T_WORD:STEEL:stal]
	[T_WORD:STERN:stafn]
	[T_WORD:STONE:steinn]
	[T_WORD:STORM:vedr]
	[T_WORD:STRANGE:kynligr]
	[T_WORD:STRAW:halmr]
	[T_WORD:STRENGTH:orka]
	[T_WORD:STRIKE:drepa]
	[T_WORD:STROKE:skellr]
	[T_WORD:SUFFER:bola]
	[T_WORD:SUMMER:sumar]
	[T_WORD:SUN:rodull]
	[T_WORD:SUPPER:nattverdr]
	[T_WORD:SWAMP:myrr]
	[T_WORD:SWEAT:sveiti]
	[T_WORD:SWEET:svass]
	[T_WORD:SWIFT:fljotr]
	[T_WORD:SWIM:svima]
	[T_WORD:SWORD:sverd]

	[T_WORD:TAKE:fa]
	[T_WORD:TALE:mal]
	[T_WORD:TERROR:hraezla]
	[T_WORD:THIRSTY:byrstr]
	[T_WORD:THRALL:braell]
	[T_WORD:THREE:prir]
	[T_WORD:THROAT:ost]
	[T_WORD:THROW:bregda]
	[T_WORD:THUNDER:brumdi]
	[T_WORD:TIME:hrid]
	[T_WORD:TOOTH:tonn]
	[T_WORD:TOUCH:taka]
	[T_WORD:TREASURE:gorsimi]
	[T_WORD:TREATY:rettr]
	[T_WORD:TROUBLE:erfidi]
	[T_WORD:TWIST:rida]
	[T_WORD:TWO:tvier]

	[T_WORD:UGLY:ljotr]
	[T_WORD:UNCLE:fodurbrodir]
	[T_WORD:USELESS:onytr]

	[T_WORD:VIOLENCE:atganga]
	[T_WORD:VOICE:rodd]

	[T_WORD:WAIL:veina]
	[T_WORD:WAR:ofridr]
	[T_WORD:WARRIOR:rekkr]
	[T_WORD:WATCH:vordr]
	[T_WORD:WAVE:unnr]
	[T_WORD:WEAR:hafa]
	[T_WORD:WEAVING:vefr]
	[T_WORD:WEIGH:vega]
	[T_WORD:WESTWARD:vestr]
	[T_WORD:WHEAT:hveiti]
	[T_WORD:WHISPER:kvisa]
	[T_WORD:WHITE:hvitr]
	[T_WORD:WICKED:vandr]
	[T_WORD:WILL:vili]
	[T_WORD:WING:vaengr]
	[T_WORD:WINTER:vetr]
	[T_WORD:WOOD:skogr]
	[T_WORD:WORK:orka]
	[T_WORD:WRATH:modr]
	[T_WORD:WRETCHED:armr]

	[T_WORD:YEAR:ar]
	[T_WORD:YOUTH:aeska]

	[T_WORD:EARTH:fold]
	[T_WORD:BED:bedr]
	[T_WORD:COMMON:karl]
	[T_WORD:DREAM:draumr]
	[T_WORD:COPPER:eir]
	[T_WORD:BRONZE:eir]
	[T_WORD:FARM:bu]
	[T_WORD:FIELD:akr]
	[T_WORD:GLACIER:jokull]
	[T_WORD:LAND:land]
	[T_WORD:MOUNTAIN:fjall]
	[T_WORD:PLAIN:vollr]
	[T_WORD:POINT:oddr]
	[T_WORD:RUSH:rasa]
	[T_WORD:SLAP:sletta]
	[T_WORD:SLEEVE:ermr]
	[T_WORD:SPECK:flekkr]
	[T_WORD:TEST:raun]
	[T_WORD:WEALTH:audaefi]
	[T_WORD:WORLD:verold]
	[T_WORD:DIMENSION:vegr]
	[T_WORD:UNIVERSE:orid]
	[T_WORD:HONEST:orgrandr]
	[T_WORD:KINGDOM:riki]
	[T_WORD:DIRECT:styra]
	[T_WORD:CLASH:songr]
	[T_WORD:SPEAK:maela]
	[T_WORD:CONTROL:vald]
	[T_WORD:BANK:bakki]
	[T_WORD:DRIVE:hrinda]
	[T_WORD:GLORY:agaeti]
	[T_WORD:RADIANCE:ljomi]
	[T_WORD:LABOR:forvirki]
	[T_WORD:SLIP:skreppa]
	[T_WORD:REQUIRE:burfa]
	[T_WORD:COUNSEL:rad]
	[T_WORD:PLAY FUN:leika]
	[T_WORD:PLAY PERFORM:efla]
	[T_WORD:GREATEST:mestr]
	[T_WORD:LEAST:minstr]
	[T_WORD:BEARD:gron]
	[T_WORD:DREAD:otti]
	[T_WORD:WALL:balkr]
	[T_WORD:TAX:gjald]
	[T_WORD:WATER:logr]
	[T_WORD:WINE:vin]
	[T_WORD:SHOVE:hrinda]
	[T_WORD:GLITTER:glita]
	[T_WORD:RUN:rinna]
	[T_WORD:ROAR:glymja]
	[T_WORD:HOLLOW:holr]
	[T_WORD:FULL:allsekr]
	[T_WORD:EMPTY:rydja]
	[T_WORD:GLAD:gladr]
	[T_WORD:BELL:bjalla]
	[T_WORD:BELLY:kvidr]
	[T_WORD:HEAVEN:himinn]
	[T_WORD:BURST:bresta]
	[T_WORD:TRIFLE:litilraedi]
	[T_WORD:RANK:rod]
	[T_WORD:BITTER:herfiligr]
	[T_WORD:DANGER:haski]
	[T_WORD:UNSWERVING:rettr]
	[T_WORD:DIVIDE:skipta]
	[T_WORD:MALIGN SLANDER V:nida]
	[T_WORD:TRUST:hlita]
	[T_WORD:CLOSE:kaerr]
	[T_WORD:WAY:leid]
	[T_WORD:MONSTER:hvedrungr]
	[T_WORD:IGNORANT:ovitr]
	[T_WORD:PELT HIDE:hud]
	[T_WORD:RIGHT BETTER:betri]
	[T_WORD:FAIR COLOR:fagr]
	[T_WORD:LOYAL:hollr]
	[T_WORD:HELP:fullting]
	[T_WORD:FRIEND:vinr]
	[T_WORD:CHEERFUL:reifr]
	[T_WORD:SALUTE:kvedja]
	[T_WORD:NATION:bjod]
	[T_WORD:DRINK:drykkr]
	[T_WORD:FLIGHT:flaugun]
	[T_WORD:GUILD:gildi]
	[T_WORD:MERCHANT:kaupmadr]
	[T_WORD:FOLD:grind]

	[T_WORD:SOLITARY:oquil]
	[T_WORD:BEND:puja]
	[T_WORD:BLAME:shen]
	[T_WORD:BLAMELESS:olat]
	[T_WORD:TWIG:ak]
	[T_WORD:GIRDER:gulo]
	[T_WORD:TRUSS:asthi]
	[T_WORD:SKEWER:istpi]
	[T_WORD:SCULPT:uthra]
	[T_WORD:TRESS:tiqua]
	[T_WORD:LOCK HAIR:onec]
	[T_WORD:CURL:luki]
	[T_WORD:TUFT:thuthu]
	[T_WORD:MEAN NASTY:strem]
	[T_WORD:MEAN LOW:gol]
	[T_WORD:SHAME:sur]
	[T_WORD:ASHAMED:engasp]
	[T_WORD:STEAM:xaked]
	[T_WORD:FERN:senre]
	[T_WORD:FERRY:cosla]
	[T_WORD:GULLY:ithra]
	[T_WORD:WORRY:vit]
	[T_WORD:LION:san]
	[T_WORD:CACTUS:stasbo]
	[T_WORD:DESK:edu]
	[T_WORD:BOTTLE:ceru]
	[T_WORD:SHINGLE:busl�]
	[T_WORD:BLANKET:kado]
	[T_WORD:CLOUD:atal]
	[T_WORD:PLATE:mori]
	[T_WORD:ROAD:udal]
	[T_WORD:HOUSE:ettad]
	[T_WORD:BEAN:seto]
	[T_WORD:BLOCK DEFEND:eslik]
	[T_WORD:BLOCK SQUARE:xugot]
	[T_WORD:FOUNTAIN:subi]
	[T_WORD:PEARL:kima]
	[T_WORD:PEAR:onmo]
	[T_WORD:THIMBLE:thrun]
	[T_WORD:BUNNY:nusbi]
	[T_WORD:ROPE:idil]
	[T_WORD:STRING:ip]
	[T_WORD:WIRE:pis]
	[T_WORD:LASH:ramkal]
	[T_WORD:SORCERY:cespi]
	[T_WORD:SORCERER:cadi]
	[T_WORD:ENSORCEL:cenan]
	[T_WORD:BEWITCH:mithrup]
	[T_WORD:MAGICIAN:ontha]
	[T_WORD:CONJURE:ispran]
	[T_WORD:CONJURER:astri]
	[T_WORD:ENCHANT:tadin]
	[T_WORD:ENCHANTER:amkash]
	[T_WORD:LANCE:angir]
	[T_WORD:LANCER:�ita]
	[T_WORD:STINK:smath]
	[T_WORD:FRECKLE:icop]
	[T_WORD:PIMPLE:slege]
	[T_WORD:WART:vuk]
	[T_WORD:BLOTCH:hocu]
	[T_WORD:CALLUS:acmot]
	[T_WORD:WIND STORM:alu]
	[T_WORD:WIND CLOCK:kortil]
	[T_WORD:HAZE:asu]
	[T_WORD:SKIN:lubbe]
	[T_WORD:SHIN:destis]
	[T_WORD:TORTURE:manoth]
	[T_WORD:TOOL:shem]
	[T_WORD:SCHOLAR:rush�n]
	[T_WORD:WEEP:oda]
	[T_WORD:MASSIVE:don]
	[T_WORD:LARGE:lod]
	[T_WORD:SMALL:semo]
	[T_WORD:LOAF:dubmith]
	[T_WORD:TILE:lir]
	[T_WORD:SPORT:ol]
	[T_WORD:SEARCH:uxen]
	[T_WORD:STALE:lolpo]
	[T_WORD:FRESH:esem]
	[T_WORD:RUB:onu]
	[T_WORD:WORTHY:alle]
	[T_WORD:WORTHLESS:ubteng]
	[T_WORD:FRIGHT:umtha]
	[T_WORD:REVOLTING:ishush]
	[T_WORD:DISGUST:mesgel]
	[T_WORD:OUTRAGE:ratad]
	[T_WORD:CRUEL:kitur]
	[T_WORD:DEVIANT:gath]
	[T_WORD:DEVIOUS:tusus]
	[T_WORD:FABULOUS:taro]
	[T_WORD:MORALITY:co�ar]
	[T_WORD:IMMORALITY:puva]
	[T_WORD:SKIN VERB:kateng]
	[T_WORD:SKINNY:kila]
	[T_WORD:NARROW:eslat]
	[T_WORD:GRIEF:beth�s]
	[T_WORD:BELT:xetan]
	[T_WORD:SANDAL:kuda]
	[T_WORD:FUTURE:kenom]
	[T_WORD:PAST:adre]
	[T_WORD:PRESENT:ocna]
	[T_WORD:BITE:guki]
	[T_WORD:SPLIT:paro]
	[T_WORD:BRASH:pethar]
	[T_WORD:BRAZEN:thrasos]
	[T_WORD:STRANGLE:r�lid]
	[T_WORD:GALLOWS:lotash]
	[T_WORD:MUSCLE:sina]
	[T_WORD:DIM:o�ir]
	[T_WORD:SPURT:gopet]
	[T_WORD:GRAVEL:rud]
	[T_WORD:RISK:sirep]
	[T_WORD:CHANCE:ishas]
	[T_WORD:CHANNEL:innah]
	[T_WORD:MIRTH:mebas]
	[T_WORD:GLEE:raji]
	[T_WORD:VICTIM:nesa]
	[T_WORD:STRANGER:vanod]
	[T_WORD:DRAB:duwar]
	[T_WORD:VISION:kisnast]
	[T_WORD:VISIONARY:sekel]
	[T_WORD:ORACLE:ledir]
	[T_WORD:UTTER:getak]
	[T_WORD:DECIDE:secen]
	[T_WORD:DECISIVE:luld�n]
	[T_WORD:SPECIAL:�om]
	[T_WORD:AUTHOR:sorus]
	[T_WORD:AUTHORITY:dosheb]
	[T_WORD:PORTENT:mudung]
	[T_WORD:MOMENTOUS:gebbis]
	[T_WORD:INFALLIBLE:anram]
	[T_WORD:AMBIGUOUS:rere]
	[T_WORD:CURIOUS:colli]
	[T_WORD:OBSCURE:mobmu]
	[T_WORD:OBSCENE:agho]
	[T_WORD:OBSTACLE:nedun]
	[T_WORD:VULGAR:nermum]
	[T_WORD:OMINOUS:uthing]
	[T_WORD:ISLAND:ethra]
	[T_WORD:CHURCH:laspar]
	[T_WORD:TEMPLE:olum]
	[T_WORD:CULT:gocta]
	[T_WORD:COVEN:kacnu]
	[T_WORD:SECT:calthath]
	[T_WORD:ORDER GROUP:cika]
	[T_WORD:CREED:alim]
	[T_WORD:COMMUNION:o�i]
	[T_WORD:DOCTRINE:ge�o]
	[T_WORD:FAITH:kel]
	[T_WORD:DENOMINATION:jath]
	[T_WORD:TEMPLE HEAD:milo]
	[T_WORD:OCCULT:othron]
	[T_WORD:ORDER CONCEPT:kor]
	[T_WORD:FAITH LOYALTY:cilba]
	[T_WORD:GUILT:nuc]
	[T_WORD:BABY:badu]
	[T_WORD:BLEED:siser]
	[T_WORD:GIRL:anba]
	[T_WORD:BOY:omo]
	[T_WORD:WOMAN:genam]
	[T_WORD:FLAG:githa]
	[T_WORD:BANNER:dilol]
	[T_WORD:WREATH:iqua]
	[T_WORD:STANDARD FLAG:sinur]
	[T_WORD:TRUMPET:quazo]
	[T_WORD:GOAL:irol]
	[T_WORD:JAIL:cudal]
	[T_WORD:PRISON:kathroc]
	[T_WORD:LIBRARY:adith]
	[T_WORD:AVALANCHE:eggu]
	[T_WORD:STIGMA:zabip]
	[T_WORD:SLICK:ivik]
	[T_WORD:TALK:enna]
	[T_WORD:JAILER:spat�on]
	[T_WORD:STILL UNMOVING:ar]
	[T_WORD:CALM:er]
	[T_WORD:HUSH:espr�]
	[T_WORD:LACONIC:lopuk]
	[T_WORD:LULL:othla]
	[T_WORD:MUTE:oquoh]
	[T_WORD:NOISELESS:kash]
	[T_WORD:QUIESCENT:osmah]
	[T_WORD:QUIET:a�a]
	[T_WORD:RETICENT:shethbah]
	[T_WORD:SATURNINE:themsol]
	[T_WORD:SPEECHLESS:erdap]
	[T_WORD:TACITURN:tethak]
	[T_WORD:FORTUNE LUCK:jalew]
	[T_WORD:FORTUNE WEALTH:bestra]
	[T_WORD:ACCIDENT:kepfu]
	[T_WORD:CIRCUMSTANCE:speski]
	[T_WORD:CONTINGENT:minat]
	[T_WORD:CONTINGENCY:kisho]
	[T_WORD:COINCIDENCE:nani]
	[T_WORD:FLUKE:sulthu]
	[T_WORD:LUCK:ralin]
	[T_WORD:CERTAIN:ceshshed]
	[T_WORD:UNCERTAIN:akmol]
	[T_WORD:FORTUITOUS:daka]
	[T_WORD:INCIDENTAL:alpeth]
	[T_WORD:AMUSE:the�i]
	[T_WORD:PASTIME:bale]
	[T_WORD:DISTRACT ANNOY:pesli]
	[T_WORD:DISTRACTION GAME:pevit]
	[T_WORD:DIVERSION GAME:irsi]
	[T_WORD:DIVERT DETOUR:sliri]
	[T_WORD:RECREATION GAME:doce]
	[T_WORD:COMPETE:cadap]
	[T_WORD:CONTEST:caslu]
	[T_WORD:MATCH CONTEST:luth]
	[T_WORD:MATCH EQUAL:ala]
	[T_WORD:TOURNAMENT:riloth]
	[T_WORD:PLAN:zapas]
	[T_WORD:SCENARIO:sufol]
	[T_WORD:STRATEGY:ormol]
	[T_WORD:TACTIC:xaki]
	[T_WORD:AUTONOMY:quistra]
	[T_WORD:EMANCIPATE:quesa]
	[T_WORD:EXTRICATE:bushik]
	[T_WORD:IMPUNITY:gognav]
	[T_WORD:LIBERATE:mapo]
	[T_WORD:LIBERTY:ilda]
	[T_WORD:RELEASE:sasir]
	[T_WORD:RELIEVE:slada]
	[T_WORD:EVEN:dasmir]
	[T_WORD:BALANCE:zostra]
	[T_WORD:EQUITY:ather]
	[T_WORD:EQUIVALENCE:kedom]
	[T_WORD:PARITY:lega]
	[T_WORD:STASIS:luhmat]
	[T_WORD:SYMMETRY:ngekil]
	[T_WORD:EQUAL:emoth]
	[T_WORD:HARMONY:ebir]
	[T_WORD:NEUTRALIZE:quice]
	[T_WORD:NEUTRAL:quabu]
	[T_WORD:STABLE UNCHANGING:manba]
	[T_WORD:LULL MISLEAD:toquem]
	[T_WORD:DOUR:wala]
	[T_WORD:GLOOM:puwog]
	[T_WORD:SEVERE:uthal]
	[T_WORD:GLUM:dolra]
	[T_WORD:IMAGE:stran]
	[T_WORD:SHRINE:lipul]
	[T_WORD:SANCTUARY:dari]
	[T_WORD:MONASTERY:vusdom]
	[T_WORD:CONVENT:ellum]
	[T_WORD:CATHEDRAL:kaslun]
	[T_WORD:CHAPEL:gaval]
	[T_WORD:SANCTUM:tomca]
	[T_WORD:INFERNO:thrathnu]
	[T_WORD:CONFLAGRATION:othral]
	[T_WORD:FLARE:akkar]
	[T_WORD:HEAT:kara]
	[T_WORD:SAND:zar]
	[T_WORD:PILLAR:kanil]
	[T_WORD:COLUMN:opra]
	[T_WORD:ARCH NOUN:ulco]
	[T_WORD:GROTTO:gasin]
	[T_WORD:CAVERN:rushan]
	[T_WORD:DEPTH:siga]
	[T_WORD:SHADE:gorbe]
	[T_WORD:VEIL:naquuv]
	[T_WORD:RELIC:othdo]
	[T_WORD:UMBRA:omsos]
	[T_WORD:PHANTOM:warosp]
	[T_WORD:MURK:sedme]
	[T_WORD:FAINT INTENSITY:saquo]
	[T_WORD:FAINT VERB:ohu]
	[T_WORD:SOMBER:saning]
	[T_WORD:TENEBROUS:geso]
	[T_WORD:MOROSE:slurac]
	[T_WORD:LOVER:ano]
	[T_WORD:BELOVED:jomsa]
	[T_WORD:ESCORT:thrut]
	[T_WORD:SUITOR:i�i]
	[T_WORD:PUNGENT:wakox]
	[T_WORD:MUSTY:slemar]
	[T_WORD:INFECT:fek]
	[T_WORD:LESION:goce]
	[T_WORD:AFFLICT:doge]
	[T_WORD:ACHE:kitew]
	[T_WORD:EPIDEMIC:spotug]
	[T_WORD:FIGHT:usel]
	[T_WORD:SCUFFLE:ngosp]
	[T_WORD:YAWN:otnge]
	[T_WORD:ACTION:ibon]
	[T_WORD:ASSAULT:okgush]
	[T_WORD:CARNAL:elloz]
	[T_WORD:STRIFE:lic]
	[T_WORD:COMBAT:thec]
	[T_WORD:CITADEL:sathar]
	[T_WORD:CITY:eno]
	[T_WORD:TOWN:ricgo]
	[T_WORD:VILLAGE:ugan]
	[T_WORD:MANOR:mekgos]
	[T_WORD:MANSION:moto]
	[T_WORD:PALACE:belza]
	[T_WORD:COTTAGE:esu]
	[T_WORD:SHACK:dak]
	[T_WORD:HUT:paru]
	[T_WORD:HOVEL:shukbu]
	[T_WORD:BASTION:shodec]
	[T_WORD:FURNACE:nganiz]
	[T_WORD:ANVIL:utdar]
	[T_WORD:BRIDGE:ori]
	[T_WORD:SAFE:icen]
	[T_WORD:GOAD:osmul]
	[T_WORD:MACHINE:tumwist]
	[T_WORD:FLAY:kigok]
	[T_WORD:OIL:konli]
	[T_WORD:SUICIDE:kur]
	[T_WORD:MARBLE BALL:pamud]
	[T_WORD:BAIT:edquek]
	[T_WORD:LURE:kixi]
	[T_WORD:REWARD:rakel]
	[T_WORD:WARD:kawe]
	[T_WORD:PRICE:owba]
	[T_WORD:PRINCE:stredac]
	[T_WORD:PRINCESS:arul]
	[T_WORD:TEMPT:ri�ih]
	[T_WORD:SPURN:saja]
	[T_WORD:SCORN:mitstu]
	[T_WORD:CONTEMPT:sopstu]
	[T_WORD:AFFECTION:elu]
	[T_WORD:COMPASSION:unpa]
	[T_WORD:SACRIFICE:mido]
	[T_WORD:CHARITY:bethri]
	[T_WORD:CHERISH:isha]
	[T_WORD:TRAMPLE:oxa]
	[T_WORD:MALICE:spuxi]
	[T_WORD:HATRED:imcor]
	[T_WORD:SPITE:mevus]
	[T_WORD:SHORE:eslo]
	[T_WORD:BEACH:thefin]
	[T_WORD:COAST:aco]
	[T_WORD:INSECT:esmma]
	[T_WORD:BUG:zusmo]
	[T_WORD:CRITTER:nikom]
	[T_WORD:BRUTE:mogsut]
	[T_WORD:PET:pathril]
	[T_WORD:BARBARIAN:rorte]
	[T_WORD:HARSH:aksu]
	[T_WORD:PRISTINE:bel]
	[T_WORD:FEROCITY:kerleb]
	[T_WORD:BARBARITY:rithreh]
	[T_WORD:FERAL:behgon]
	[T_WORD:FURY:ngira]
	[T_WORD:RUTHLESS:paboz]
	[T_WORD:GORE NOUN:gistrib]
	[T_WORD:CHAMPION:zilar]
	[T_WORD:VIRTUE:otu]
	[T_WORD:SCANDAL:bakki]
	[T_WORD:APOGEE:iwa]
	[T_WORD:SPIRE:papos]
	[T_WORD:SPIRAL:pictham]
	[T_WORD:CLIMAX:serid]
	[T_WORD:CLIMATE:quinbor]
	[T_WORD:WEATHER:se�am]
	[T_WORD:CREST:pirni]
	[T_WORD:CULMINATE:ospram]
	[T_WORD:CUSP:kopka]
	[T_WORD:MOST:dom]
	[T_WORD:PEAK:uquur]
	[T_WORD:BLUNT:honu]
	[T_WORD:ROOF:fat�b]
	[T_WORD:CEILING:anri]
	[T_WORD:FLOOR:dolak]
	[T_WORD:SUMMIT:suril]
	[T_WORD:TIP:mec]
	[T_WORD:TOP:acal]
	[T_WORD:BOTTOM:tuthu]
	[T_WORD:SIDE:ibid]
	[T_WORD:ZENITH:ata]
	[T_WORD:FLANK VERB:ic]
	[T_WORD:PASS MOUNTAIN:stren]
	[T_WORD:PASS VERB:duko]
	[T_WORD:PASSAGE:ehil]
	[T_WORD:CORRIDOR:rura]
	[T_WORD:HALL:gatshi]
	[T_WORD:GALLERY:oj�]
	[T_WORD:GALLEY:dem]
	[T_WORD:ARENA:lomam]
	[T_WORD:ARMORY:namar]
	[T_WORD:ASSEMBLE:corust]
	[T_WORD:MEET:jamas]
	[T_WORD:THEATER:meben]
	[T_WORD:ENTRANCE:simin]
	[T_WORD:EXIT:suwu]
	[T_WORD:ENTRY:neni]
	[T_WORD:VESTIBULE:strohe]
	[T_WORD:FROST:tirin]
	[T_WORD:FEVER:okol]
	[T_WORD:HUMID:ussam]
	[T_WORD:HUMOR:belom]
	[T_WORD:COMEDY:sithe]
	[T_WORD:SIZZLE:anstrid]
	[T_WORD:SWELTER:rulu]
	[T_WORD:WARM:tul]
	[T_WORD:TORRID:ushnil]
	[T_WORD:ACRID:slinam]
	[T_WORD:DALE:nihde]
	[T_WORD:DELL:l�lcil]
	[T_WORD:GLADE:shama]
	[T_WORD:VALE:thola]
	[T_WORD:BASIN:rulak]
	[T_WORD:BOWL:ba�ec]
	[T_WORD:CAVITY:ulum]
	[T_WORD:CRATER:urwa]
	[T_WORD:DEN:mogem]
	[T_WORD:DENT:dof]
	[T_WORD:DEPRESS SAD:bowu]
	[T_WORD:DEPRESSION LOW:melbe]
	[T_WORD:DIMPLE:lapip]
	[T_WORD:DIP:irne]
	[T_WORD:DISH:ushcen]
	[T_WORD:EXCAVATE:menung]
	[T_WORD:GROOVE:nacu]
	[T_WORD:GULF SEA:ulde]
	[T_WORD:GULF PIT:rono]
	[T_WORD:GULF DISTANCE:aggung]
	[T_WORD:DISTANCE:bedo]
	[T_WORD:NOTCH:amec]
	[T_WORD:SAG:bomu]
	[T_WORD:SCOOP:kunod]
	[T_WORD:SOCKET:inspuz]
	[T_WORD:TROUGH:daseb]
	[T_WORD:VALLEY:wom]
	[T_WORD:FENCE:iguk]
	[T_WORD:BARRICADE:gosath]
	[T_WORD:BLOCKADE:sothbod]
	[T_WORD:BOARD PLANK:adu]
	[T_WORD:BOARD GET ON:sagmo]
	[T_WORD:PLANK:bini]
	[T_WORD:DEFEND:joddo]
	[T_WORD:DEFENSE:talde]
	[T_WORD:DIKE:tunul]
	[T_WORD:PALISADE:�wlist]
	[T_WORD:POST:jol]
	[T_WORD:RAMPART:rirdest]
	[T_WORD:STAKE:kopoh]
	[T_WORD:STOP:kiros]
	[T_WORD:STOCKADE:katslet]
	[T_WORD:ABYSS:nomar]
	[T_WORD:CHASM:ustres]
	[T_WORD:CREVICE:utesh]
	[T_WORD:RIFT:xim]
	[T_WORD:FISSURE:ishes]
	[T_WORD:UNDER:ngir]
	[T_WORD:ABYSMAL:usca]
	[T_WORD:SUBMERGE:nohus]
	[T_WORD:CYCLONE:zurko]
	[T_WORD:TYPHOON:kafek]
	[T_WORD:HURRICANE:satheth]
	[T_WORD:GALE:irka]
	[T_WORD:TORNADO:idri]
	[T_WORD:HAIL GREET:reme]
	[T_WORD:HAIL ICE:gelu]
	[T_WORD:HALE:ashcir]
	[T_WORD:ROBUST:atup]
	[T_WORD:HARDY:vost]
	[T_WORD:VIGOR:bok]
	[T_WORD:AIR:idem]
	[T_WORD:SPLASH:ofo]
	[T_WORD:DABBLE:bekdil]
	[T_WORD:DOUSE:togir]
	[T_WORD:DRENCH:ispsug]
	[T_WORD:MOIST:sedast]
	[T_WORD:PLUNGE:nulce]
	[T_WORD:SHOWER:ethlal]
	[T_WORD:SLOP:quihu]
	[T_WORD:SLOSH:uki]
	[T_WORD:SOAK:budzu]
	[T_WORD:SPATTER:samspi]
	[T_WORD:SPLATTER:gethdaz]
	[T_WORD:SPRAY:taba]
	[T_WORD:SPREAD:pos]
	[T_WORD:SPRINKLE:cosin]
	[T_WORD:TWINKLE:istra]
	[T_WORD:SQUIRT:tuthru]
	[T_WORD:WAD:dap]
	[T_WORD:WADE:huro]
	[T_WORD:WET:ukor]
	[T_WORD:FADE:lomu]
	[T_WORD:COLORLESS:otod]
	[T_WORD:BLANCH:oprig]
	[T_WORD:BLEACH:thremlu]
	[T_WORD:BLENCH:vesh]
	[T_WORD:FLINCH:cac]
	[T_WORD:APPEAR:u�ir]
	[T_WORD:DISAPPEAR:norod]
	[T_WORD:DISSOLVE:xat]
	[T_WORD:DULL:nid]
	[T_WORD:EVAPORATE:kelkif]
	[T_WORD:LUSTER:icgil]
	[T_WORD:TONE:imbo]
	[T_WORD:VANISH:vithba]
	[T_WORD:WASH:tega]
	[T_WORD:ABATE:nogu]
	[T_WORD:DECLINE:zengtho]
	[T_WORD:DETERIORATE:sulak]
	[T_WORD:DIMINISH:mota]
	[T_WORD:DISPERSE:jori]
	[T_WORD:DROOP:smoma]
	[T_WORD:DWINDLE:dugvur]
	[T_WORD:KINDLE:radi]
	[T_WORD:STOKE:ezif]
	[T_WORD:POKE:cobi]
	[T_WORD:STIR:lemhuh]
	[T_WORD:FEED:lapa]
	[T_WORD:FAIL:nusko]
	[T_WORD:LANGUISH:str�th]
	[T_WORD:LESSEN:odod]
	[T_WORD:LESSON:teme]
	[T_WORD:TEACH:rossu]
	[T_WORD:MASTERY:bastsan]
	[T_WORD:PERISH:zulshag]
	[T_WORD:SHRIVEL:shasik]
	[T_WORD:SINK:ewom]
	[T_WORD:TAPER:meka]
	[T_WORD:THIN:isac]
	[T_WORD:TIRE:sug]
	[T_WORD:WANE:dolil]
	[T_WORD:WEAK:rith]
	[T_WORD:WILT:nathob]
	[T_WORD:WITHER:omre]
	[T_WORD:HONOR:uthros]
	[T_WORD:ADORE:emim]
	[T_WORD:ADULATE:tu�ik]
	[T_WORD:CELEBRATE:shanum]
	[T_WORD:CONFIDENT:izem]
	[T_WORD:DEFERENCE:arod]
	[T_WORD:DEITY:ahang]
	[T_WORD:DISTINCT:thili]
	[T_WORD:ELEVATE:geral]
	[T_WORD:ESTEEM:anlar]
	[T_WORD:EXALT:zoka]
	[T_WORD:FAME:rogon]
	[T_WORD:FEALTY:mathras]
	[T_WORD:HOMAGE:upnal]
	[T_WORD:MORTAL:�lat]
	[T_WORD:IMMORTAL:athri]
	[T_WORD:LAUD:ilo]
	[T_WORD:OBEISANCE:degnu]
	[T_WORD:PRAISE:on]
	[T_WORD:PRESTIGE:piral]
	[T_WORD:RENOWN:pessal]
	[T_WORD:REPUTATION:thinir]
	[T_WORD:TRIBUTE:bendi]
	[T_WORD:WORSHIP:isman]
	[T_WORD:CHASTITY:laro]
	[T_WORD:COURAGE:sol]
	[T_WORD:DECENT:risas]
	[T_WORD:GOOD:are]
	[T_WORD:INNOCENT:astan]
	[T_WORD:MODEST:orda]
	[T_WORD:PRINCIPLE:bal]
	[T_WORD:PURE:kemsa]
	[T_WORD:RIGHTEOUSNESS:lihost]
	[T_WORD:TRUTH:cusal]
	[T_WORD:TRUTHFUL:ada]
	[T_WORD:VIRGIN:cisli]
	[T_WORD:VIRGINITY:esmmi]
	[T_WORD:HOLD:fel]
	[T_WORD:SCALD:ronik]
	[T_WORD:SCALE VERB:thalo]
	[T_WORD:SCALE SKIN:thep]
	[T_WORD:CLAN:gabat]
	[T_WORD:LABYRINTH:nethrez]
	[T_WORD:MAZE:espir]
	[T_WORD:WEB:mete]
	[T_WORD:CROWD:gospo]
	[T_WORD:FAMILY:abla]
	[T_WORD:KIN:sisha]
	[T_WORD:MOB:shasttol]
	[T_WORD:ORGAN:stama]
	[T_WORD:ORGANIZE:jestri]
	[T_WORD:MESH:upek]
	[T_WORD:TANGLE:spibsa]
	[T_WORD:ENTANGLE:spepip]
	[T_WORD:PUZZLE:necar]
	[T_WORD:PERPLEX:dencoth]
	[T_WORD:QUANDARY:daslut]
	[T_WORD:COMBINE:abpa]
	[T_WORD:FLICKER:eve]
	[T_WORD:BRAND:ebbak]
	[T_WORD:SINGE:thetal]
	[T_WORD:SEAR:adoth]
	[T_WORD:SCORCH:belrok]
	[T_WORD:ROAST:stoltad]
	[T_WORD:PARCH:nesim]
	[T_WORD:IGNITE:streti]
	[T_WORD:INCINERATE:tedaz]
	[T_WORD:GLOW:quemer]
	[T_WORD:CREMATE:thrimes]
	[T_WORD:FACTION:ozo]
	[T_WORD:CREW:�esik]
	[T_WORD:VESSEL:emeg]
	[T_WORD:LEAGUE GROUP:thaguk]
	[T_WORD:COOPERATE:ame]
	[T_WORD:GROUP:besti]
	[T_WORD:GANG:dotep]
	[T_WORD:FELLOWSHIP:dasar]
	[T_WORD:PARTNER:zemel]
	[T_WORD:RIDDLE:pestrat]
	[T_WORD:ROUT:lasod]
	[T_WORD:SCRAPE:bujit]
	[T_WORD:SCRAP:pob]
	[T_WORD:SCOUR:iclo]
	[T_WORD:SCRUB:desa]
	[T_WORD:SHAKE:aloc]
	[T_WORD:SOAP:kamven]
	[T_WORD:SPONGE:muma]
	[T_WORD:SWEEP:begi]
	[T_WORD:WHISKER:zega]
	[T_WORD:FASTEN:bithar]
	[T_WORD:WINNOW:nosta]
	[T_WORD:WIPE:ladgi]
	[T_WORD:CLENCH:thuslax]
	[T_WORD:CLINCH:dipug]
	[T_WORD:CLOUT:obol]
	[T_WORD:CLUTCH:cero]
	[T_WORD:DOMINATE:utast]
	[T_WORD:DOMINION:alnos]
	[T_WORD:GRASP:asmur]
	[T_WORD:INFLUENCE:thur]
	[T_WORD:OWN:rodoh]
	[T_WORD:OWNERSHIP:pusap]
	[T_WORD:TENACITY:uzo]
	[T_WORD:BIND:uti]
	[T_WORD:CARRY:ucaf]
	[T_WORD:CATCH:�ik]
	[T_WORD:CONFINE:tokda]
	[T_WORD:CONTAIN:puti]
	[T_WORD:CRADLE:wuspin]
	[T_WORD:EMBRACE:cusith]
	[T_WORD:POOL:nique]
	[T_WORD:SOCIETY:thespde]
	[T_WORD:CIRCLE:aslu]
	[T_WORD:SQUARE:aquov]
	[T_WORD:TRIANGLE:amic]
	[T_WORD:CONNECT:geb]
	[T_WORD:MERGE:lecbe]
	[T_WORD:CONFUSE:devsem]
	[T_WORD:COMPLEX:nedul]
	[T_WORD:MEANDER:tapstren]
	[T_WORD:MUDDLE:nothra]
	[T_WORD:BEWILDER:quamun]
	[T_WORD:MORASS:milub]
	[T_WORD:PROBLEM:strospi]
	[T_WORD:INTRICATE:naspa]
	[T_WORD:FLUSH:thudas]
	[T_WORD:MOP:gom]
	[T_WORD:POLISH:eko]
	[T_WORD:PURGE:caku]
	[T_WORD:RASP:boki]
	[T_WORD:RINSE:co�e]
	[T_WORD:BAND OBJECT:mones]
	[T_WORD:BAND GROUP:osda]
	[T_WORD:TALL:rismu]
	[T_WORD:SHORT:pasub]
	[T_WORD:BULBOUS:lucog]
	[T_WORD:BULB:timik]
	[T_WORD:ALLY:abba]
	[T_WORD:ALLIANCE:betca]
	[T_WORD:ALLEGIANCE:nubi]
	[T_WORD:COALITION:lusko]
	[T_WORD:COAL:shud]
	[T_WORD:ENJOY:almo]
	[T_WORD:FONDLE:dapen]
	[T_WORD:HANDLE OBJECT:mim]
	[T_WORD:HANDLE VERB:samo]
	[T_WORD:IMPRISON:usmik]
	[T_WORD:NOURISH:apo]
	[T_WORD:SQUEEZE:laroth]
	[T_WORD:TRAMMEL:ushus]
	[T_WORD:VICE:bazsa]
	[T_WORD:VISE:k�d]
	[T_WORD:DEFECT FAULT:ngab]
	[T_WORD:PERFECT:esmin]
	[T_WORD:GEM:ves]
	[T_WORD:JEWEL:cobim]
	[T_WORD:WIELD:ir]
	[T_WORD:WRING:ismig]
	[T_WORD:CONJUNCTION:stalith]
	[T_WORD:BLOT:nastrisp]
	[T_WORD:BLOW:uslig]
	[T_WORD:BRUSH:puji]
	[T_WORD:CLARIFY:luc]
	[T_WORD:CLEANSE:lelku]
	[T_WORD:DREDGE:sethag]
	[T_WORD:ERASE:slecol]
	[T_WORD:MORSEL:pasmug]
	[T_WORD:MONGREL:wuvul]
	[T_WORD:TENDER:amsan]
	[T_WORD:URGE:gasom]
	[T_WORD:MECHANISM:jepum]
	[T_WORD:SMEAR:rir]
	[T_WORD:TATTOO:�absiz]
	[T_WORD:WEATHER VERB:equo]
	[T_WORD:CRUX:boshkuc]
	[T_WORD:PEEK:leru]
	[T_WORD:SNEAK:rorkek]
	[T_WORD:ATTIC:pidud]
	[T_WORD:BASEMENT:jonic]
	[T_WORD:DEARTH:ngahen]
	[T_WORD:APEX:sted]
	[T_WORD:NADIR:cugo]
	[T_WORD:TUNNEL:gujeg]
	[T_WORD:ENTRANCE VERB:ulo]
	[T_WORD:DELIGHT:kemus]
	[T_WORD:JOY:ebka]
	[T_WORD:FRIGID:ponoc]
	[T_WORD:ANKLE:ikar]
	[T_WORD:ELBOW:sesle]
	[T_WORD:PASSION:hethre]
	[T_WORD:ARDENT:rotec]
	[T_WORD:STREAM:ebi]
	[T_WORD:BOWEL GUT:ngilsho]
	[T_WORD:BOWEL INTERIOR:istrul]
	[T_WORD:DRILL BORE:strul]
	[T_WORD:DRILL ROUTINE:arat]
	[T_WORD:BORE DRILL:aguv]
	[T_WORD:BOREDOM:ro�ad]
	[T_WORD:FLECK:ipiz]
	[T_WORD:SPOT:lurit]
	[T_WORD:DOT:catet]
	[T_WORD:CREATE:sekur]
	[T_WORD:POEM:oli]
	[T_WORD:POETRY:saquar]
	[T_WORD:POET:pimra]
	[T_WORD:SCRIBE:dosla]
	[T_WORD:MONK:emung]
	[T_WORD:HERMIT:mothec]
	[T_WORD:RECLUSE:zinga]
	[T_WORD:NAME:ucim]
	[T_WORD:LANGUAGE:ashthal]
	[T_WORD:PHRASE:gido]
	[T_WORD:RHYME:wisho]
	[T_WORD:RHYTHM:bortu]
	[T_WORD:VERSE:godan]
	[T_WORD:DRUM VERB:arad]
	[T_WORD:CREATURE:mos]
	[T_WORD:SNAIL:copgur]
	[T_WORD:VINE:gili]
	[T_WORD:SQUASH:malstral]
	[T_WORD:NEVER:usmo]
	[T_WORD:DWELL:etus]
	[T_WORD:HABIT:tup]
	[T_WORD:CHEW:mipbi]
	[T_WORD:SWALLOW:cocu]
	[T_WORD:DISEMBOWEL:cawcul]
	[T_WORD:EVISCERATE:mogast]
	[T_WORD:RAPID:beso]
	[T_WORD:MEDICINE:samas]
	[T_WORD:HEAL:asla]
	[T_WORD:SELL:taram]
	[T_WORD:MARKET:poshi]
	[T_WORD:SLOW:puwun]
	[T_WORD:MIRE:moquu]
	[T_WORD:ADMIRE:emsi]
	[T_WORD:DIRGE:xusta]
	[T_WORD:LAMENT:uwshe]
	[T_WORD:FUNERAL:sor]
	[T_WORD:QUEST:akul]
	[T_WORD:ADVENTURE:rithod]
	[T_WORD:MOURN:ransa]
	[T_WORD:MORNING:sabu]
	[T_WORD:BULWARK:buslon]
	[T_WORD:COSMOS:ana]
	[T_WORD:ANGUISH:�od]
	[T_WORD:TAUT:espo]
	[T_WORD:DEIFY:shadu]
	[T_WORD:INFAMY:cogith]
	[T_WORD:MORTALITY:ejel]
	[T_WORD:IMMORTALITY:camu]
	[T_WORD:MORTIFY:ngojo]
	[T_WORD:OBEY:ceshca]
	[T_WORD:WORSHIPPER:pabpath]
	[T_WORD:RIDDLE HOLES:gigol]
	[T_WORD:DWELLING:odda]
	[T_WORD:DISEMBOWELMENT:awthrar]
	[T_WORD:EVISCERATION:esxac]
	[T_WORD:HEALING:onbir]
	[T_WORD:ADMIRATION:lami]
	[T_WORD:CONFLICT:sesta]
	[T_WORD:ATTACK:nashra]
	[T_WORD:ONSLAUGHT:gencesh]
	[T_WORD:TREE:akan]
	[T_WORD:AMBER:kuppo]
	[T_WORD:AMETHYST:zebna]
	[T_WORD:AQUA:boha]
	[T_WORD:AQUAMARINE:tihsa]
	[T_WORD:GRAY:zido]
	[T_WORD:AUBURN:gusa]
	[T_WORD:AZURE:edlal]
	[T_WORD:BEIGE:the�ic]
	[T_WORD:BRASS:athod]
	[T_WORD:BROWN:tasar]
	[T_WORD:BUFF:se�id]
	[T_WORD:SIENNA:rossi]
	[T_WORD:UMBER:shedosh]
	[T_WORD:CARDINAL_COLOR:shigo]
	[T_WORD:CARMINE:buz�ith]
	[T_WORD:CERULEAN:rosced]
	[T_WORD:CHARCOAL:casast]
	[T_WORD:CHARTREUSE:erquin]
	[T_WORD:CHESTNUT:cubra]
	[T_WORD:CHOCOLATE:molcet]
	[T_WORD:CINNAMON:ronu]
	[T_WORD:COBALT:bote]
	[T_WORD:INDIGO:kakthril]
	[T_WORD:OLIVE:lunda]
	[T_WORD:PINK:itlip]
	[T_WORD:SCARLET:thecek]
	[T_WORD:TAN:dalic]
	[T_WORD:ECRU:runi]
	[T_WORD:EMERALD:hehan]
	[T_WORD:FLAX:cegad]
	[T_WORD:FUCHSIA:gicthosm]
	[T_WORD:GOLDENROD:xaddul]
	[T_WORD:HELIOTROPE:cingshu]
	[T_WORD:LAVENDER:deshsul]
	[T_WORD:BLUSH:do�as]
	[T_WORD:LEMON:gebdum]
	[T_WORD:LILAC:shal]
	[T_WORD:LIME:vuli]
	[T_WORD:MAHOGANY:quoshas]
	[T_WORD:MAROON_COLOR:athru]
	[T_WORD:MAUVE:poreb]
	[T_WORD:TAUPE:upril]
	[T_WORD:MINT:quaka]
	[T_WORD:MOSS:zih]
	[T_WORD:OCHRE:segok]
	[T_WORD:ORANGE:omthu]
	[T_WORD:PERIWINKLE:xiza]
	[T_WORD:PLUM:ube]
	[T_WORD:PUCE:zepboc]
	[T_WORD:PUMPKIN:munosh]
	[T_WORD:RUSSET:ubhuth]
	[T_WORD:SAFFRON:shezpa]
	[T_WORD:SEPIA:theh]
	[T_WORD:TEAL:muwi]
	[T_WORD:TURQUOISE:ki��]
	[T_WORD:VERMILION:aszed]
	[T_WORD:PATH:anur]
	[T_WORD:THREAT:cihir]
	[T_WORD:WARNING:gistri]
	[T_WORD:FATE:sutar]
	[T_WORD:DESTINY:tetha]
	[T_WORD:HORROR:vuthe]
	[T_WORD:LITTLE:istel]
	[T_WORD:EARLY:rashed]
	[T_WORD:LATE:cuthu]
	[T_WORD:LIFE:kimen]
	[T_WORD:CHEESE:anig]
	[T_WORD:FIGURE_OBJECT:udi]
	[T_WORD:HOPE:dana]
	[T_WORD:BODY:shalo]
	[T_WORD:WALK:oma]
	[T_WORD:HOUR:ocba]
	[T_WORD:SIT:aslap]
	[T_WORD:MOMENT:mistir]
	[T_WORD:BEAUTY:sattha]
	[T_WORD:JUDGE:�or]
	[T_WORD:WISH:quosa]
	[T_WORD:RESPECT:medon]
	[T_WORD:MARK:kesp]
	[T_WORD:HEAVY:thogsloc]
	[T_WORD:TRIAL:wure]
	[T_WORD:PRACTICE:dopod]
